# Qr [![pipeline status](https://gitlab.com/llinksrechts/qr/badges/master/pipeline.svg)](https://gitlab.com/llinksrechts/qr/commits/master) [![coverage](https://gitlab.com/llinksrechts/qr/badges/master/coverage.svg)](https://gitlab.com/llinksrechts/qr/commits/master)
Rust re-implementation of [KuhTap](https://github.com/Neverbolt/q), a stack based, reverse polish, turing complete programming language that consists of the letter `q` and the control character `\t`.

## Installation
### Requirements
Before building, grab the latest version of Rust using [rustup](https://rustup.rs/).
### Building
* Clone the project

  ```bash
  git clone git@gitlab.com:llinksrechts/qr.git
  cd qr
  ```
* Compiling/Installing

  ```bash
  cargo install
  ```
* \[OPTIONAL\] Running tests

  ```bash
  cargo test
  ```
Make sure you have `$HOME/.cargo/bin` added to your `$PATH`.

## Usage
### REPL
```bash
qr
```
### Execute program
```bash
qr <program> [arguments]
```

## Documentation
* [Source documentation](https://qr.lukor.org/qr)
* [Syntax](syntax.bnf)
* [Standard library](stdlib.md)
* [Error codes](errorcodes.md)

## Contributing
For a guide on how to contribute to this project, see [CONTRIBUTING.md](CONTRIBUTING.md).

## Changelog
The changelog can be found in [CHANGELOG](CHANGELOG).

## License
This project is available under the [MIT License](LICENSE).

## Authors
* [Lukas Rysavy](https://home.lukor.org/)
