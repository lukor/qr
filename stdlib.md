# Standard library

| Name | Function |
|------|----------|
| **q.\*** | **Standard library** |
| q.a | print |
| q.b | println |
| q.c | read stdin |
| q.d | error |
| q.e | fatal error |
| **q.q.\*** | **math** |
| q.q.a | float pow (valid for negative and decimal exponents, outputs a float) |
| q.q.b | mod pow (0^1 mod 2) |
| q.q.c | sqrt |
| q.q.d | round |
| q.q.e | ceil |
| q.q.f | floor |
| q.q.g | ln |
| q.q.h | log10 |
| q.q.i | abs |
| **q.q.q.\*** | **trigonometry** |
| q.q.q.a | sin |
| q.q.q.b | cos |
| q.q.q.c | tan |
| q.q.q.d | asin |
| q.q.q.e | acos |
| q.q.q.f | atan |
| q.q.q.g | sinh |
| q.q.q.h | cosh |
| q.q.q.i | tanh |
| q.q.q.j | asinh |
| q.q.q.k | acosh |
| q.q.q.l | atanh |
| **q.qq.\*** | **subprocess** |
| q.qq.a | eval bash -> stack\[0\] = number of args; stack\[1..i\] = args |
| q.qq.b | eval bash + output |
| **q.qqq.\*** | **file io** |
| q.qqq.a | file open(mode, path) with mode in 1:read,2:write,3:append |
| q.qqq.b | file read(fid, bytes), all for negative bytes |
| q.qqq.c | file write(fid, content), same behavior as print |
| q.qqq.d | file close(fid) |
| q.qqq.e | import |
| **q.qqqq.\*** | **string** |
| q.qqqq.a | length |
| q.qqqq.b | contains/count |
| q.qqqq.c | replace |
| q.qqqq.d | split, pushes string parts and count |
| q.qqqq.e | join, opposite of split |
| q.qqqq.f | to lower |
| q.qqqq.g | to upper |
| q.qqqq.h | trim |
| q.qqqq.i | from byte array |
| q.qqqq.j | to byte array |
| **q.qqqqq.\*** | **network** for each: address either hostname -> string or ip -> 4 numbers, data: any data type until null, will be converted to bytes |
| **q.qqqqq.q.\*** | **udp** |
| q.qqqqq.q.a | create socket (port) |
| q.qqqqq.q.b | send package (port, destination address, destination port, data) |
| q.qqqqq.q.c | receive package (port) |
| q.qqqqq.q.d | close socket (port) |
| **q.qqqqq.qq.\*** | **tcp** |
| q.qqqqq.qq.a | connect (destination address, destination port) |
| q.qqqqq.qq.b | listen (port) |
| q.qqqqq.qq.c | send (connection id, data) |
| q.qqqqq.qq.d | receive (connection id) |
| q.qqqqq.qq.e | accept (port) |
| q.qqqqq.qq.f | close connection |
