# Error classes
* `0....` - normal errors
  * `01...` - stack/data
    * `010..` - stack operations
      * `0100.` - get/move
      * `0101.` - pop
    * `011..` - cast
    * `011..` - cast
  * `02...` - primitive ops
    * `029..` - compare
  * `03...` - io
    * `031..` - file
    * `032..` - udp
    * `033..` - tcp
    * `034..` - stdio
  * `09...` - invalid block operations
    * `090..` - function creation
    * `091..` - escape (return/break/continue)
    * `093..` - block control
    * `099..` - function call
* `10...` - unknown commands/operations/...
  * `100..` - commands
  * `101..` - operations
  * `102..` - data arguments
* `9....` - interpreter error
  * `98...` - handler error