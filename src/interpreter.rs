use handlers::{Handler, RootHandler};
use memory::stack::Stack;
use memory::heap::Heap;
use memory::data::{Null, Num};
use commands::Escape;
use std::io::BufReader;
use std::fs::File;

/// An interpreter for QR
pub struct QRInterpreter {
    handler: Option<RootHandler>,
    pub stack: Stack,
    pub heap: Heap,
    just_executed: bool,
}

/// An interpreter for q based languages
pub trait Interpreter {
    /// Adds a token to the interpreter, usually called by the parser
    ///
    /// # Arguments
    /// * `len` - the length of the token
    ///
    /// # Returns
    /// A file reader if code was imported, None otherwise
    fn add_token(&mut self, len: u64) -> Option<BufReader<File>>;
    /// Checks whether an instruction was just executed
    ///
    /// # Returns
    /// `true` if the last token lead to an execution
    fn executed(&mut self) -> bool;
    /// Resets the interpreter state
    fn reset(&mut self);
    /// Adds command line parameters to the interpreter
    ///
    /// # Arguments
    /// * `params` - a list of parameters
    fn add_parameters(&mut self, params: Vec<String>);
}

impl Interpreter for QRInterpreter {
    fn add_token(&mut self, len: u64) -> Option<BufReader<File>> {
        self.just_executed = false;
        let mut ret = None;
        if len > 0 {
            if self.handler.as_mut().unwrap().add_argument(len) {
                let handler = self.handler.take().unwrap();
                match handler.execute(&mut self.stack, &mut self.heap) {
                    Escape::None => (),
                    Escape::Execute(reader) => { ret = Some(reader) },
                    escape => escape.error(),
                };
                self.handler = Some(RootHandler::new());
                self.just_executed = true;
            }
        }
        ret
    }

    fn executed(&mut self) -> bool {
        self.just_executed
    }

    fn reset(&mut self) {
        self.handler = Some(RootHandler::new());
        self.just_executed = true;
    }
    fn add_parameters(&mut self, params: Vec<String>) {
        let len = params.len() as i64;
        for param in params {
            self.stack.push(Box::new(Null));
            self.stack.push_multiple(str_to_data_vec!(param.chars()));
        }
        self.stack.push(Box::new(Num::from(len)));
    }
}

impl QRInterpreter {
    /// Creates a new QRInterpreter
    ///
    /// # Returns
    /// The newly created interpreter
    pub fn new() -> Self {
        QRInterpreter {
            handler: Some(RootHandler::new()),
            stack: Stack::new(),
            heap: Heap::new(),
            just_executed: true,
        }
    }
}
