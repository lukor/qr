/// Macros
#[macro_use]
mod macros;
/// Macros for error handling
#[macro_use]
mod error;

/// Utility functions
pub mod util;
/// Program parser
pub mod parser;
/// Memory related code (data, stack, heap)
pub mod memory;
/// Executable commands
pub mod commands;
/// Interpreter token handlers
pub mod handlers;
/// Code interpreter
pub mod interpreter;
/// Standard library
pub mod stdlib;
