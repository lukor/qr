use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::Heap;
use memory::data::Dtype;

/// Command handling additions
#[derive(Clone)]
pub struct AddCommand;

impl Command for AddCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.add(&second);
        #[cfg(debug_assertions)]
            println!("Add {} + {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling subtractions
#[derive(Clone)]
pub struct SubCommand;

impl Command for SubCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.sub(&second);
        #[cfg(debug_assertions)]
            println!("Sub {} - {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling multiplications
#[derive(Clone)]
pub struct MulCommand;

impl Command for MulCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.mul(&second);
        #[cfg(debug_assertions)]
            println!("Mul {} * {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling divisions
#[derive(Clone)]
pub struct DivCommand;

impl Command for DivCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.div(&second);
        #[cfg(debug_assertions)]
            println!("Div {} / {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the `modulo` operator
#[derive(Clone)]
pub struct ModCommand;

impl Command for ModCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.rem(&second);
        #[cfg(debug_assertions)]
            println!("Mod {} % {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the bitwise `and` operator
#[derive(Clone)]
pub struct AndCommand;

impl Command for AndCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.and(&second);
        #[cfg(debug_assertions)]
            println!("And {} & {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the bitwise `or` operator
#[derive(Clone)]
pub struct OrCommand;

impl Command for OrCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.or(&second);
        #[cfg(debug_assertions)]
            println!("Or {} | {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the bitwise `exclusive or` operator
#[derive(Clone)]
pub struct XorCommand;

impl Command for XorCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.xor(&second);
        #[cfg(debug_assertions)]
            println!("Xor {} ^ {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the bitwise `not` operator
#[derive(Clone)]
pub struct NotCommand;

impl Command for NotCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let elem = stack.pop_one();
        let result = elem.bit_not();
        #[cfg(debug_assertions)]
            println!("Not !{} = {}", elem, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the `power` operator
#[derive(Clone)]
pub struct PowCommand;

impl Command for PowCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.pow(&second);
        #[cfg(debug_assertions)]
            println!("Pow {} ^ {} = {}", first, second, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the action of getting the data type of a value
#[derive(Clone)]
pub struct GetDtypeCommand;

impl Command for GetDtypeCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let result;
        {
            let element = stack.get(1);
            result = Box::new(Dtype::from(element.get_data_type()));
            #[cfg(debug_assertions)]
                println!("type({}) = {}", element, result);
        }
        stack.push(result);
        Escape::None
    }
}
