use memory::data::Data;
use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::Heap;

/// Command handling the `push` operation
#[derive(Clone)]
pub struct PushCommand {
    value: Vec<Box<Data>>,
}

impl Command for PushCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        for value in &self.value {
            #[cfg(debug_assertions)]
                println!("Push {}", value);
            stack.push(value.clone());
        }
        Escape::None
    }
}

impl PushCommand {
    /// Creates a new PushCommand with the given value to push
    ///
    /// # Arguments
    ///
    /// * `value` - Value to push
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(value: Vec<Box<Data>>) -> Self {
        Self { value }
    }
}

/// Options for how many elements to pop
#[derive(Debug, Clone)]
pub enum PopAction {
    /// Any amount specified in self.0
    Many(u64),
    /// Until the next `NULL`-value
    String,
    /// Read the first value on the stack, then treat as Many(value)
    Stack,
}

/// Command handling the `pop` operation
#[derive(Clone)]
pub struct PopCommand {
    amount: PopAction,
}

impl Command for PopCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        #[cfg(debug_assertions)]
            println!("Pop {:?}", self.amount);
        stack.pop(&self.amount);
        Escape::None
    }
}

impl PopCommand {
    /// Creates a new PopCommand with the given value to push
    ///
    /// # Arguments
    ///
    /// * `action` - How to pop values
    ///   * `1` - Pop one
    ///   * `2` - Pop multiple
    ///   * `3` - Pop string (see [PopAction](enum.PopAction.html#variants))
    ///   * `4` - Get pop amount from stack
    /// * `amount` - How many values to pop if action = 2
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(action: u8, amount: Option<u64>) -> Self {
        PopCommand { amount: match action {
            1u8 => PopAction::Many(1),
            2u8 => PopAction::Many(amount.unwrap()),
            3u8 => PopAction::String,
            4u8 => PopAction::Stack,
            _ => error!(91011),
        }}
    }
}

/// Command handling the `get` operation (copying a value to the top of the stack)
#[derive(Clone)]
pub struct GetCommand;

impl Command for GetCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let index = stack.pop_one().as_num() as u64;
        #[cfg(debug_assertions)]
            println!("Get {}", index);
        if index < 1 {
            error!(1001, index);
        }
        let new_data = stack.get(index).clone();
        stack.push(new_data);
        Escape::None
    }
}

/// Command handling the `get string` operation (copying a string to the top of the stack)
#[derive(Clone)]
pub struct GetStrCommand;

impl Command for GetStrCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let index = stack.pop_one().as_num() as u64;
        #[cfg(debug_assertions)]
            println!("Get str {}", index);
        if index < 1 {
            error!(1001, index);
        }
        let mut data_list = vec![];
        let mut i = 0;
        loop {
            let new_data = stack.get(index + i);
            data_list.push(new_data.clone());
            if new_data.is_null() {
                break;
            }
            i += 1;
        }
        data_list.reverse();
        stack.push_multiple(data_list);
        Escape::None
    }
}

/// Command handling the `move` operation (moving a value to the top of the stack)
#[derive(Clone)]
pub struct MoveCommand;

impl Command for MoveCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let index = stack.pop_one().as_num() as u64;
        #[cfg(debug_assertions)]
            println!("Move {}", index);
        if index < 2 {
            error!(1002, index);
        }
        let new_data = stack.take(index);
        stack.push(new_data);
        Escape::None
    }
}

/// Command handling the `move string` operation (moving a string to the top of the stack)
#[derive(Clone)]
pub struct MoveStrCommand;

impl Command for MoveStrCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let index = stack.pop_one().as_num() as u64;
        #[cfg(debug_assertions)]
            println!("Move str {}", index);
        if index < 2 {
            error!(1001, index);
        }
        let mut data_list = vec![];
        loop {
            let new_data = stack.take(index);
            let is_null = new_data.is_null();
            data_list.push(new_data);
            if is_null {
                break;
            }
        }
        data_list.reverse();
        stack.push_multiple(data_list);
        Escape::None
    }
}
