use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::Heap;
use memory::data::Bool;
use std::cmp::Ordering;

/// Command handling the boolean `not` operation
#[derive(Clone)]
pub struct NotCommand;

impl Command for NotCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let elem = stack.pop_one();
        let result = elem.not();
        #[cfg(debug_assertions)]
            println!("Not !{} = {}", elem, result);
        stack.push(result);
        Escape::None
    }
}

/// Command handling the comparison operation
#[derive(Clone)]
pub struct EqCommand;

impl Command for EqCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.cmp(&second) == Ordering::Equal;
        #[cfg(debug_assertions)]
            println!("Eq {} == {} = {}", first, second, result);
        stack.push(Box::new(Bool::from(result)));
        Escape::None
    }
}

/// Command handling the inverse of the comparison operation
#[derive(Clone)]
pub struct NeqCommand;

impl Command for NeqCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.cmp(&second) != Ordering::Equal;
        #[cfg(debug_assertions)]
            println!("Neq {} != {} = {}", first, second, result);
        stack.push(Box::new(Bool::from(result)));
        Escape::None
    }
}

/// Command handling the `less than`-operation
#[derive(Clone)]
pub struct LtCommand;

impl Command for LtCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.cmp(&second) == Ordering::Less;
        #[cfg(debug_assertions)]
            println!("Lt {} < {} = {}", first, second, result);
        stack.push(Box::new(Bool::from(result)));
        Escape::None
    }
}

/// Command handling the `greater than`-operation
#[derive(Clone)]
pub struct GtCommand;

impl Command for GtCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.cmp(&second) == Ordering::Greater;
        #[cfg(debug_assertions)]
            println!("Gt {} > {} = {}", first, second, result);
        stack.push(Box::new(Bool::from(result)));
        Escape::None
    }
}

/// Command handling the `less than or equal`-operation
#[derive(Clone)]
pub struct LteCommand;

impl Command for LteCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.cmp(&second) != Ordering::Greater;
        #[cfg(debug_assertions)]
            println!("Lte {} <= {} = {}", first, second, result);
        stack.push(Box::new(Bool::from(result)));
        Escape::None
    }
}

/// Command handling the `greater than or equal`-operation
#[derive(Clone)]
pub struct GteCommand;

impl Command for GteCommand {
    fn execute(&self, stack: &mut Stack, _heap: &mut Heap) -> Escape {
        let second = stack.pop_one();
        let first = stack.pop_one();
        let result = first.cmp(&second) != Ordering::Less;
        #[cfg(debug_assertions)]
            println!("Gte {} >= {} = {}", first, second, result);
        stack.push(Box::new(Bool::from(result)));
        Escape::None
    }
}
