use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::{Heap, FuncRet};

/// Command handling a function call
#[derive(Clone)]
pub struct CallCommand;

impl Command for CallCommand {
    fn execute(&self, stack: &mut Stack, heap: &mut Heap) -> Escape {
        let ns = stack.pop_one().as_ns().clone();
        let name = stack.pop_one().clone();
        #[cfg(debug_assertions)]
            println!("Calling function {}.{}", ns.iter().map(|i| num_as_q!(*i)).collect::<Vec<_>>().join("."), name);
        let mut function = heap.get_fn(ns, name).clone();
        function.call(stack, heap);
        match function.get_ret() {
            FuncRet::Execute(reader) => Escape::Execute(reader),
            _ => Escape::None,
        }
    }
}
