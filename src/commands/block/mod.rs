/// Commands for control sequences
pub mod control;
/// Commands for calling functions
pub mod call;

use commands::{Command, Escape};
use memory::stack::Stack;
use memory::heap::{Heap, QRFunction};
use memory::data::{Data, Namespace};

/// Command handling an if block
#[derive(Clone)]
pub struct IfCommand {
    child_commands: Vec<Box<Command>>,
}

impl Command for IfCommand {
    fn execute(&self, stack: &mut Stack, heap: &mut Heap) -> Escape {
        if stack.get(1).as_bool() {
            #[cfg(debug_assertions)]
                println!("Executing if branch");
            for command in &self.child_commands {
                match command.execute(stack, heap) {
                    Escape::None => (),
                    x => return x,
                }
            }
        } else {
            #[cfg(debug_assertions)]
                println!("Not executing if branch");
        }
        Escape::None
    }
}

impl IfCommand {
    /// Creates a new IfCommand with the given block children
    ///
    /// # Arguments
    ///
    /// * `child_commands` - Children of the if block
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(child_commands: Vec<Box<Command>>) -> Self {
        IfCommand { child_commands }
    }
}

/// Command handling a while block
#[derive(Clone)]
pub struct WhileCommand {
    child_commands: Vec<Box<Command>>,
}

impl Command for WhileCommand {
    fn execute(&self, stack: &mut Stack, heap: &mut Heap) -> Escape {
        while stack.get(1).as_bool() {
            #[cfg(debug_assertions)]
                println!("Executing while branch");
            for command in &self.child_commands {
                match command.execute(stack, heap) {
                    Escape::None => (),
                    Escape::Break(amount) => {
                        if amount == 1 {
                            #[cfg(debug_assertions)]
                                println!("Breaking from while branch");
                            return Escape::None;
                        }
                        return Escape::Break(amount - 1);
                    }
                    Escape::Continue(amount) => {
                        if amount == 1 {
                            break;
                        }
                        return Escape::Continue(amount - 1);
                    }
                    x => return x,
                }
            }
        }
        #[cfg(debug_assertions)]
            println!("Done executing while branch");
        Escape::None
    }
}

impl WhileCommand {
    /// Creates a new WhileCommand with the given block children
    ///
    /// # Arguments
    ///
    /// * `child_commands` - Children of the while block
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(child_commands: Vec<Box<Command>>) -> Self {
        WhileCommand { child_commands }
    }
}

/// Command handling creation of a function block
#[derive(Clone)]
pub struct FuncCommand {
    child_commands: Vec<Box<Command>>,
}

impl Command for FuncCommand {
    fn execute(&self, stack: &mut Stack, heap: &mut Heap) -> Escape {
        let ns = stack.get(1).as_ns().clone();
        if ns.len() < 1 {
            error!(99001);
        }
        if ns[0] == 1 {
            error!(9001);
        }
        let name = stack.get(2).clone();
        self.execute_for_name(heap, ns, name)
    }
}

impl FuncCommand {
    /// Creates a new FuncCommand with the given block children
    ///
    /// # Arguments
    ///
    /// * `child_commands` - Children of the function block
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(child_commands: Vec<Box<Command>>) -> Self {
        FuncCommand { child_commands }
    }

    /// Creates the function with the given namespace and name
    ///
    /// # Arguments
    ///
    /// * `heap` - The heap to write the function to
    /// * `ns` - The namespace to write the function to
    /// * `name` - The desired name of the function
    ///
    /// # Returns
    /// An escape code to signal if the command continues, breaks or returns from parent blocks or executes a new code stream
    pub fn execute_for_name(&self, heap: &mut Heap, ns: Vec<u64>, name: Box<Data>) -> Escape {
        if !name.can_be_func_name() {
            error!(9011, format!("{:?}", name.get_data_type()));
        }
        let func = QRFunction::new(self.child_commands.clone());
        #[cfg(debug_assertions)]
            println!("Adding function {} to namespace {:?}", name, ns);
        heap.add_fn(ns, name, Box::new(func));
        Escape::None
    }
}

/// Command handling creation of an anonymous function block
#[derive(Clone)]
pub struct AnonFuncCommand {
    func_command: FuncCommand,
}

impl Command for AnonFuncCommand {
    fn execute(&self, stack: &mut Stack, heap: &mut Heap) -> Escape {
        let name = heap.generate_func_id();
        stack.push(name.clone());
        stack.push(Box::new(Namespace::from(vec![1])));
        self.func_command.execute_for_name(heap, vec![1], name)
    }
}

impl AnonFuncCommand {
    /// Creates a new AnonFuncCommand with the given block children
    ///
    /// # Arguments
    ///
    /// * `child_commands` - Children of the function block
    ///
    /// # Returns
    ///
    /// The newly created command
    pub fn new(child_commands: Vec<Box<Command>>) -> Self {
        AnonFuncCommand { func_command: FuncCommand::new(child_commands) }
    }
}
