use memory::stack::Stack;
use memory::heap::Heap;
use std::clone::Clone;
use std::io::BufReader;
use std::fs::File;

/// Commands for stack operations
pub mod stack;
/// Commands for primitive operations
pub mod ops;
/// Commands for comparison operations
pub mod comp;
/// Commands for blocks
pub mod block;
/// Commands for casting
pub mod cast;

/// Enum containing various escape scenarios for commands
pub enum Escape {
    /// The command does not manipulate outside blocks
    None,
    /// The command wants to skip the current iteration of the n<sup>th</sup> loop
    Continue(u64),
    /// The command wants to break out of the n<sup>th</sup> loop
    Break(u64),
    /// The command wants to return from the current function
    Return,
    /// The command wants to execute code from the given stream
    Execute(BufReader<File>),
}

impl Escape {
    /// Raises the appropriate error message for an invalid escape
    pub fn error(&self) {
        match self {
            &Escape::Continue(_) => error!(9101),
            &Escape::Break(_) => error!(9102),
            &Escape::Return => error!(9103),
            _ => error!(99101),
        };
    }
}

/// An executable command
pub trait Command: CommandClone {
    /// Executes the command using the given stack and heap, may return an escape code
    ///
    /// # Arguments
    ///
    /// * `stack` - The stack to use
    /// * `heap` - The heap to use
    ///
    /// # Returns
    /// An escape code to signal if the command continues, breaks or returns from parent blocks or executes a new code stream
    fn execute(&self, stack: &mut Stack, heap: &mut Heap) -> Escape;
}

/// Wrapper for command to make them clonable inside of boxes
pub trait CommandClone {
    /// Clones a box containing a Command
    fn clone_box(&self) -> Box<Command>;
}

impl<T> CommandClone for T where T: 'static + Command + Clone {
    fn clone_box(&self) -> Box<Command> {
        Box::new(self.clone())
    }
}

impl Clone for Box<Command> {
    fn clone(&self) -> Box<Command> {
        self.clone_box()
    }
}
