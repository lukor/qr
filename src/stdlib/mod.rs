/// Functions for writing to stdout
pub mod print;
/// Functions for raising errors
pub mod error;
/// Mathematical functions
pub mod math;
/// Functions for executing external commands
pub mod subprocess;
/// Functions for handling files
pub mod file;
/// String operation functions
pub mod string;
/// Functions for handling network connections
pub mod network;

use memory::stack::Stack;
use memory::heap::{Heap, Function};
use memory::data::Null;
use std::io::stdin;

/// Function to read from stdin
#[derive(Clone)]
pub struct ReadStdinFunction;

impl Function for ReadStdinFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let mut data = "".to_string();
        match stdin().read_line(&mut data) {
            Err(error) => stack.push(error!(3401, error)),
            Ok(_) => (),
        };
        stack.push(Box::new(Null));
        let len = data.len() - 1;
        data.truncate(len);
        stack.push_multiple(str_to_data_vec!(data.chars()));
    }
}
