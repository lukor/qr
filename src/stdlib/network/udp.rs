use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::{Num, DataType};
use std::net::UdpSocket;

/// Function to open a UDP socket
#[derive(Clone)]
pub struct OpenUdpSocketFunction;

impl Function for OpenUdpSocketFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let port = stack.pop_one().as_num() as u16;
        let socket = match UdpSocket::bind(format!("0.0.0.0:{port}", port=port)) {
            Ok(s) => s,
            Err(reason) => {
                stack.push(error!(3211, reason));
                return;
            },
        };
        heap.open_udp_socket(port, socket);
    }
}

/// Function to send data over UDP
#[derive(Clone)]
pub struct SendUdpFunction;

impl Function for SendUdpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let port = stack.pop_one().as_num() as u16;
        let dest_addr;
        if stack.get(1).get_data_type() == DataType::Char {
            dest_addr = fmt_data_vec!(stack.pop_str());
        } else {
            let mut parts = [0u8; 4];
            for i in 0..4 {
                parts[i] = stack.pop_one().as_num() as u8;
            }
            dest_addr = parts.iter().map(|part| format!("{}", part)).collect::<Vec<_>>().join(".");
        }
        let dest_port = stack.pop_one().as_num() as u16;
        let socket = heap.get_udp_socket(port);
        let data = data_vec_to_byte_array!(stack.pop_str());
        match socket.send_to(&data, format!("{addr}:{port}", addr=dest_addr, port=dest_port)) {
            Ok(_) => (),
            Err(reason) => stack.push(error!(3212, dest_addr, dest_port, reason)),
        }
    }
}

/// Function to receive data over UDP
#[derive(Clone)]
pub struct ReceiveUdpFunction;

impl Function for ReceiveUdpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let port = stack.pop_one().as_num() as u16;
        let socket = heap.get_udp_socket(port);
        let mut buf = [0; 2048];
        let (number_of_bytes, src_addr) = match socket.recv_from(&mut buf) {
            Err(error) => {
                stack.push(error!(3213, error));
                return;
            },
            Ok(data) => data,
        };
        stack.push_multiple(byte_array_to_data_vec!(buf, number_of_bytes));
        stack.push(Box::new(Num::from(number_of_bytes as i64)));
        stack.push_multiple(str_to_data_vec!(src_addr.to_string().to_owned().chars()));
    }
}

/// Function to close an open UDP socket
#[derive(Clone)]
pub struct CloseUdpSocketFunction;

impl Function for CloseUdpSocketFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let port = stack.pop_one().as_num() as u16;
        match heap.close_udp_socket(port) {
            Err(error) => stack.push(error),
            Ok(_) => (),
        }
    }
}
