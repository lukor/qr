use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::{Null, Num, DataType};
use std::io::prelude::*;
use std::net::{TcpStream, TcpListener};

/// Function to connect over TCP
#[derive(Clone)]
pub struct ConnectTcpFunction;

impl Function for ConnectTcpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let dest_addr;
        if stack.get(1).get_data_type() == DataType::Char {
            dest_addr = fmt_data_vec!(stack.pop_str());
        } else {
            let mut parts = [0u8; 4];
            for i in 0..4 {
                parts[i] = stack.pop_one().as_num() as u8;
            }
            dest_addr = parts.iter().map(|part| format!("{}", part)).collect::<Vec<_>>().join(".");
        }
        let dest_port = stack.pop_one().as_num() as u16;

        let stream = match TcpStream::connect(format!("{addr}:{port}", addr=dest_addr, port=dest_port)) {
            Ok(stream) => stream,
            Err(reason) => {
                stack.push(error!(3311, dest_addr, dest_port, reason));
                return;
            },
        };
        stack.push(Box::new(Num::from(heap.open_tcp_stream(stream) as i64)));
    }
}

/// Function to listen on a TCP port
#[derive(Clone)]
pub struct ListenTcpFunction;

impl Function for ListenTcpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let port = stack.pop_one().as_num() as u16;
        let listener = match TcpListener::bind(format!("127.0.0.1:{}", port)) {
            Ok(listener) => listener,
            Err(reason) => {
                stack.push(error!(3312, port, reason));
                return;
            },
        };
        heap.open_tcp_listener(port, listener);
    }
}

/// Function to send data over a TCP connection
#[derive(Clone)]
pub struct SendTcpFunction;

impl Function for SendTcpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let id = stack.pop_one().as_num() as u64;
        let data = data_vec_to_byte_array!(stack.pop_str());
        let mut stream = heap.get_tcp_stream(id);
        match stream.write(&data) {
            Ok(_) => (),
            Err(reason) => stack.push(error!(3313, reason)),
        };
    }
}

/// Function to receive data over a TCP connection
#[derive(Clone)]
pub struct ReceiveTcpFunction;

impl Function for ReceiveTcpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let id = stack.pop_one().as_num() as u64;
        let mut stream = heap.get_tcp_stream(id);
        let mut buf = [0u8; 2048];
        let bytes_read = match stream.read(&mut buf) {
            Ok(bytes) => bytes,
            Err(reason) => {
                stack.push(error!(3314, reason));
                return;
            },
        };
        stack.push_multiple(byte_array_to_data_vec!(buf, bytes_read));
        stack.push(Box::new(Num::from(bytes_read as i64)));
    }
}

/// Function to accept an incoming TCP connection
#[derive(Clone)]
pub struct AcceptTcpFunction;

impl Function for AcceptTcpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let port = stack.pop_one().as_num() as u16;
        let (stream, src_addr) = match heap.get_tcp_listener(port).accept() {
            Ok(stream) => stream,
            Err(reason) => {
                stack.push(error!(3315, port, reason));
                return;
            },
        };
        stack.push(Box::new(Num::from(heap.open_tcp_stream(stream) as i64)));
        stack.push(Box::new(Null));
        stack.push_multiple(str_to_data_vec!(src_addr.to_string().to_owned().chars()));
    }
}

/// Function to close an open TCP connection
#[derive(Clone)]
pub struct CloseTcpFunction;

impl Function for CloseTcpFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let id = stack.pop_one().as_num() as u64;
        match heap.close_tcp_stream(id) {
            Err(error) => stack.push(error),
            Ok(_) => (),
        }
    }
}
