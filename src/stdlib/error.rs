use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::DataType;

/// Function to throw an error
#[derive(Clone)]
pub struct ErrorFunction;

impl Function for ErrorFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let error_code = stack.pop_one().as_num() as u64;
        let error_message = fmt_data_vec!(stack.pop_str());
        stack.push(as_error!(error_code, error_message));
    }
}

/// Function to throw a fatal error
#[derive(Clone)]
pub struct FatalErrorFunction;

impl Function for FatalErrorFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        if stack.get(1).get_data_type() == DataType::Error {
            let error = stack.pop_one();
            fatal_error!(error.as_num(), error.as_str());
        }
        let error_code = stack.pop_one().as_num();
        let error_message = fmt_data_vec!(stack.pop_str());
        fatal_error!(error_code, error_message);
    }
}
