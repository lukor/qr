use memory::heap::{Heap, Function, FuncRet};
use memory::stack::Stack;
use memory::data::{Null, Num, DataType};
use std::fs::{OpenOptions, File};
use std::path::Path;
use std::io::prelude::*;
use std::io::BufReader;
use util;

/// Function to open a file
#[derive(Clone)]
pub struct OpenFunction;

impl Function for OpenFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let mode = stack.pop_one().as_num();
        if mode < 0 || mode > 2 {
            error!(3112, mode);
        }
        let fname = fmt_data_vec!(stack.pop_str());
        let path = Path::new(&fname);
        let display = path.display();
        let mut oo = OpenOptions::new();
        oo.read(true);
        if mode > 0 {
            oo.write(true).create(true);
        }
        if mode > 1 {
            oo.append(true);
        }
        let file = match oo.open(&path) {
            Err(reason) => {
                stack.push(error!(3111, display, reason));
                return;
            },
            Ok(file) => file,
        };
        let fid = heap.open_file(file);
        stack.push(Box::new(Num::from(fid as i64)));
    }
}

macro_rules! file_read_error {
    ($result:expr) => {
    match $result {
        Err(reason) => error!(3112, reason),
        _ => (),
    }
    };
}

/// Function to read from a file
#[derive(Clone)]
pub struct ReadFunction;

impl Function for ReadFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let fid = stack.pop_one().as_num() as u64;
        let bytes = stack.pop_one().as_num();
        let mut file = heap.get_file(fid);
        let mut content;
        if bytes < 0 {
            content = vec![];
            file_read_error!(file.read_to_end(&mut content));
        } else {
            content = vec![0u8; bytes as usize];
            file_read_error!(file.read(&mut content));
        }
        stack.push(Box::new(Null));
        stack.push_multiple(str_to_data_vec!(content));
    }
}

/// Function to write to a file
#[derive(Clone)]
pub struct WriteFunction;

impl Function for WriteFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let fid = stack.pop_one().as_num() as u64;
        let err;
        {
            let top = stack.get(1);
            err = match heap.get_file(fid).write(match top.get_data_type() {
                DataType::Char => fmt_data_vec!(stack.get_str()),
                _ => format!("{}", top),
            }.as_bytes()) {
                Err(reason) => Err(error!(3113, fid, reason)),
                _ => Ok(()),
            };
        }
        match err {
            Err(error) => stack.push(error),
            Ok(_) => (),
        }
    }
}

/// Function to close an open file
#[derive(Clone)]
pub struct CloseFunction;

impl Function for CloseFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        let fid = stack.pop_one().as_num() as u64;
        match heap.close_file(fid) {
            Err(error) => stack.push(error),
            Ok(_) => (),
        }
    }
}

/// Function to import code from a file
pub struct ImportFunction(Option<BufReader<File>>);

impl Function for ImportFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let fname = fmt_data_vec!(stack.pop_str());
        let file_reader = util::get_file_reader(&fname);
        self.0 = Some(file_reader);
    }

    fn get_ret(&mut self) -> FuncRet {
        FuncRet::Execute(self.0.take().unwrap())
    }
}

impl Clone for ImportFunction {
    fn clone(&self) -> Self {
        ImportFunction::new()
    }
}

impl ImportFunction {
    /// Creates a new ImportFunction
    ///
    /// # Returns
    /// The newly created function
    pub fn new() -> Self {
        ImportFunction(None)
    }
}
