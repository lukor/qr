use memory::heap::{Heap, Function};
use memory::stack::Stack;
use memory::data::DataType;
use std::clone::Clone;

/// Function capable of printing data
pub trait PrintingFunction: Clone + Function {
    fn print(&self, data: String);
}

impl<T> Function for T where T: 'static + PrintingFunction {
    fn call(&mut self, stack: &mut Stack, _heap: &mut Heap) {
        let top = stack.get(1);
        match top.get_data_type() {
            DataType::Char => {
                self.print(fmt_data_vec!(stack.get_str()));
            }
            _ => {
                self.print(format!("{}", top));
            }
        }
    }
}

/// Function to print data without a trailing newline
#[derive(Clone)]
pub struct PrintFunction;

impl PrintingFunction for PrintFunction {
    fn print(&self, data: String) {
        print!("{}", data);
    }
}

/// Function to print data with a trailing newline
#[derive(Clone)]
pub struct PrintlnFunction;

impl PrintingFunction for PrintlnFunction {
    fn print(&self, data: String) {
        println!("{}", data);
    }
}
