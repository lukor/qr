use commands::{Command, Escape};
use memory::data::{Data, DataType, Num, Error};
use memory::stack::Stack;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::net::{UdpSocket, TcpStream, TcpListener};
use stdlib::ReadStdinFunction;
use stdlib::print::*;
use stdlib::error::*;
use stdlib::math::*;
use stdlib::math::trig::*;
use stdlib::subprocess::*;
use stdlib::file::*;
use stdlib::string::*;
use stdlib::network::udp::*;
use stdlib::network::tcp::*;

/// A heap for functions and FDs
pub struct Heap {
    root_ns: Namespace,
    last_id: u64,
    open_files: HashMap<u64, File>,
    udp_sockets: HashMap<u16, UdpSocket>,
    tcp_streams: HashMap<u64, TcpStream>,
    tcp_listeners: HashMap<u16, TcpListener>,
}

impl Heap {
    /// Creates a new empty heap
    ///
    /// # Returns
    /// The newly created heap
    pub fn new() -> Self {
        Heap { root_ns: Namespace::root(), last_id: 0u64, open_files: HashMap::new(), udp_sockets: HashMap::new(), tcp_streams: HashMap::new(), tcp_listeners: HashMap::new() }
    }

    /// Adds a function to some namespace
    ///
    /// # Arguments
    /// * `namespace` - the namespace to add the function to
    /// * `name` - the name of the function
    /// * `func` - the executable function
    pub fn add_fn(&mut self, namespace: Vec<u64>, name: Box<Data>, func: Box<Function>) {
        self.root_ns.add_fn(namespace, name, func);
        #[cfg(debug_assertions)]
            self.print();
    }

    /// Gets a function in a namespace
    ///
    /// # Arguments
    /// * `namespace` - the namespace of the function
    /// * `name` - the name of the function
    ///
    /// # Returns
    /// The function
    pub fn get_fn(&self, namespace: Vec<u64>, name: Box<Data>) -> &Box<Function> {
        self.root_ns.get_fn(namespace, name)
    }

    /// Generates a unique id for anonymous functions
    ///
    /// # Returns
    /// The id wrapped in a Data object inside of a box
    pub fn generate_func_id(&mut self) -> Box<Data> {
        self.last_id += 1;
        Box::new(Num::from(self.last_id as i64))
    }

    /// Adds a new open file to the list
    ///
    /// # Arguments
    /// * `file` - the file to add
    ///
    /// # Returns
    /// A file handle
    pub fn open_file(&mut self, file: File) -> u64 {
        let new_id = self.open_files.keys().max().unwrap_or(&0u64)+1;
        self.open_files.insert(new_id, file);
        #[cfg(debug_assertions)]
            println!("files: {:?}", self.open_files);
        new_id
    }

    /// Gets a file from the list
    ///
    /// # Arguments
    /// * `fid` - the file handle
    ///
    /// # Returns
    /// A reference to the file
    pub fn get_file(&mut self, fid: u64) -> &File {
        match self.open_files.get(&fid) {
            Some(file) => file,
            None => error!(3101, fid),
        }
    }

    /// Closes an open file
    ///
    /// # Arguments
    /// * `fid` - the file handle
    ///
    /// # Returns
    /// Ok if the file was closed, otherwise an error
    pub fn close_file(&mut self, fid: u64) -> Result<(), Box<Error>> {
        let ret = match self.open_files.remove(&fid) {
            None => Err(error!(3102, fid)),
            _ => Ok(()),
        };
        #[cfg(debug_assertions)]
            println!("files: {:?}", self.open_files);
        ret
    }

    /// Adds a new open UDP socket to the list
    ///
    /// # Arguments
    /// * `port` - the local port of the socket
    /// * `socket` - the socket to add
    pub fn open_udp_socket(&mut self, port: u16, socket: UdpSocket) {
        self.udp_sockets.insert(port, socket);
        #[cfg(debug_assertions)]
            println!("udp: {:?}", self.udp_sockets);
    }

    /// Gets a UDP socket from the list
    ///
    /// # Arguments
    /// * `port` - the local port of the socket
    ///
    /// # Returns
    /// A reference to the socket
    pub fn get_udp_socket(&mut self, port: u16) -> &UdpSocket {
        match self.udp_sockets.get(&port) {
            Some(socket) => socket,
            None => error!(3201, port),
        }
    }

    /// Closes an open UDP socket
    ///
    /// # Arguments
    /// * `port` - the port of the socket
    ///
    /// # Returns
    /// Ok if the socket was closed, otherwise an error
    pub fn close_udp_socket(&mut self, port: u16) -> Result<(), Box<Error>> {
        let ret = match self.udp_sockets.remove(&port) {
            None => Err(error!(3202, port)),
            _ => Ok(()),
        };
        #[cfg(debug_assertions)]
            println!("udp: {:?}", self.udp_sockets);
        ret
    }

    /// Adds a new TCP listener to the list
    ///
    /// # Arguments
    /// * `port` - the port of the listener
    /// * `listener` - the listener
    pub fn open_tcp_listener(&mut self, port: u16, listener: TcpListener) {
        self.tcp_listeners.insert(port, listener);
        #[cfg(debug_assertions)]
            println!("tcpl: {:?}", self.tcp_listeners);
    }

    /// Gets a TCP listener
    ///
    /// # Arguments
    /// * `port` - the port of the listener
    ///
    /// # Returns
    /// A reference to the listener
    pub fn get_tcp_listener(&mut self, port: u16) -> &TcpListener {
        match self.tcp_listeners.get(&port) {
            Some(listener) => listener,
            None => error!(3301, port),
        }
    }

    /// Adds a new TCP stream to the list
    ///
    /// # Arguments
    /// * `stream` - the stream to add
    ///
    /// # Returns
    /// A handle for the stream
    pub fn open_tcp_stream(&mut self, stream: TcpStream) -> u64 {
        let new_id = self.tcp_streams.keys().max().unwrap_or(&0u64)+1;
        self.tcp_streams.insert(new_id, stream);
        #[cfg(debug_assertions)]
            println!("tcp: {:?}", self.tcp_streams);
        new_id
    }

    /// Gets a TCP stream
    ///
    /// # Arguments
    /// * `id` - the stream handle
    ///
    /// # Returns
    /// A reference to the stream
    pub fn get_tcp_stream(&mut self, id: u64) -> &TcpStream {
        match self.tcp_streams.get(&id) {
            Some(stream) => stream,
            None => error!(3302, id),
        }
    }

    /// Closes an open TCP stream
    ///
    /// # Arguments
    /// * `id` - the stream handle
    ///
    /// # Returns
    /// Ok if the stream was closed, otherwise an error
    pub fn close_tcp_stream(&mut self, id: u64) -> Result<(), Box<Error>> {
        let ret = match self.tcp_streams.remove(&id) {
            None => Err(error!(3303, id)),
            _ => Ok(()),
        };
        #[cfg(debug_assertions)]
            println!("tcp: {:?}", self.tcp_streams);
        ret
    }

    /// Prints all members of the heap
    #[cfg(debug_assertions)]
    pub fn print(&self) {
        println!("Heap:");
        self.root_ns.print("".to_owned());
    }
}

/// A namespace containing functions and/or child namespaces
struct Namespace {
    child_namespaces: HashMap<u64, Namespace>,
    members: HashMap<(DataType, i64), Box<Function>>,
}

impl Namespace {
    /// Creates a new namespace
    ///
    /// # Returns
    /// The newly created namespace
    pub fn new() -> Self {
        Namespace { child_namespaces: HashMap::new(), members: HashMap::new() }
    }
    /// Creates a root namespace
    ///
    /// # Returns
    /// The newly created namespace
    pub fn root() -> Self {
        Namespace::new()
            .add_ns(1, Namespace::global())
    }
    /// Creates the global stdlib namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn global() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(PrintFunction))
            .add_char_member('b', Box::new(PrintlnFunction))
            .add_char_member('c', Box::new(ReadStdinFunction))
            .add_char_member('d', Box::new(ErrorFunction))
            .add_char_member('e', Box::new(FatalErrorFunction))
            .add_ns(1, Namespace::math())
            .add_ns(2, Namespace::subprocess())
            .add_ns(3, Namespace::file())
            .add_ns(4, Namespace::string())
            .add_ns(5, Namespace::network())
    }
    /// Creates the stdlib math namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn math() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(PowFunction))
            .add_char_member('b', Box::new(ModPowFunction))
            .add_char_member('c', Box::new(SqrtFunction))
            .add_char_member('d', Box::new(RoundFunction))
            .add_char_member('e', Box::new(CeilFunction))
            .add_char_member('f', Box::new(FloorFunction))
            .add_char_member('g', Box::new(LnFunction))
            .add_char_member('h', Box::new(LogFunction))
            .add_char_member('i', Box::new(AbsFunction))
            .add_ns(1, Namespace::trig())
    }
    /// Creates the stdlib trigonometric functions namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn trig() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(SinFunction))
            .add_char_member('b', Box::new(CosFunction))
            .add_char_member('c', Box::new(TanFunction))
            .add_char_member('d', Box::new(AsinFunction))
            .add_char_member('e', Box::new(AcosFunction))
            .add_char_member('f', Box::new(AtanFunction))
            .add_char_member('g', Box::new(SinhFunction))
            .add_char_member('h', Box::new(CoshFunction))
            .add_char_member('i', Box::new(TanhFunction))
            .add_char_member('j', Box::new(AsinhFunction))
            .add_char_member('k', Box::new(AcoshFunction))
            .add_char_member('l', Box::new(AtanhFunction))
    }
    /// Creates the stdlib subprocess namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn subprocess() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(BashFunction))
            .add_char_member('b', Box::new(BashOutFunction))
    }
    /// Creates the stdlib file namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn file() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(OpenFunction))
            .add_char_member('b', Box::new(ReadFunction))
            .add_char_member('c', Box::new(WriteFunction))
            .add_char_member('d', Box::new(CloseFunction))
            .add_char_member('e', Box::new(ImportFunction::new()))
    }
    /// Creates the stdlib string namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn string() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(LenFunction))
            .add_char_member('b', Box::new(CountFunction))
            .add_char_member('c', Box::new(ReplaceFunction))
            .add_char_member('d', Box::new(SplitFunction))
            .add_char_member('e', Box::new(JoinFunction))
            .add_char_member('f', Box::new(LowerFunction))
            .add_char_member('g', Box::new(UpperFunction))
            .add_char_member('h', Box::new(TrimFunction))
            .add_char_member('i', Box::new(FromByteArrayFunction))
            .add_char_member('j', Box::new(ToByteArrayFunction))
    }
    /// Creates the stdlib network namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn network() -> Self {
        Namespace::new()
            .add_ns(1, Namespace::udp())
            .add_ns(2, Namespace::tcp())
    }
    /// Creates the stdlib udp namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn udp() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(OpenUdpSocketFunction))
            .add_char_member('b', Box::new(SendUdpFunction))
            .add_char_member('c', Box::new(ReceiveUdpFunction))
            .add_char_member('d', Box::new(CloseUdpSocketFunction))
    }
    /// Creates the stdlib tcp namespace
    ///
    /// # Returns
    /// The newly created namespace
    fn tcp() -> Self {
        Namespace::new()
            .add_char_member('a', Box::new(ConnectTcpFunction))
            .add_char_member('b', Box::new(ListenTcpFunction))
            .add_char_member('c', Box::new(SendTcpFunction))
            .add_char_member('d', Box::new(ReceiveTcpFunction))
            .add_char_member('e', Box::new(AcceptTcpFunction))
            .add_char_member('f', Box::new(CloseTcpFunction))
    }

    /// Adds a function to the namespace or one of its children
    ///
    /// # Arguments
    /// * `namespace` - the subsequent namespace path if the function should be added to a child
    /// * `name` - the name of the function
    /// * `func` - the executable function
    pub fn add_fn(&mut self, mut namespace: Vec<u64>, name: Box<Data>, func: Box<Function>) {
        if namespace.len() > 0 {
            let child_ns = self.child_namespaces.entry(namespace[0]).or_insert(Namespace::new());
            namespace.remove(0);
            child_ns.add_fn(namespace, name, func);
        } else {
            let key = (name.get_data_type(), name.as_num());
            self.members.insert(key, func);
        }
    }

    /// Adds a function to the namespace (consuming self)
    ///
    /// # Arguments
    /// * `name` - the function identifier
    /// * `func` - the executable function
    ///
    /// # Returns
    /// Self
    fn add_member(mut self, name: (DataType, i64), func: Box<Function>) -> Self {
        self.members.insert(name, func);
        self
    }

    /// Adds a function with a character name to the namespace (consuming self)
    ///
    /// # Arguments
    /// * `chr` - the function identifier
    /// * `func` - the executable function
    ///
    /// # Returns
    /// Self
    fn add_char_member(self, chr: char, func: Box<Function>) -> Self {
        self.add_member((DataType::Char, chr as u8 as i64), func)
    }

    /// Adds a child namespace (consuming self)
    ///
    /// # Arguments
    /// * `name` - the namespace identifier
    /// * `ns` - the namespace path
    ///
    /// # Returns
    /// Self
    fn add_ns(mut self, name: u64, ns: Namespace) -> Self {
        self.child_namespaces.insert(name, ns);
        self
    }

    /// Gets a function from the namespace or one of its children
    ///
    /// # Arguments
    /// * `namespace` - the subsequent namespace path if the function is a member of a child namespace
    /// * `name` - the function name
    ///
    /// # Returns
    /// A reference to the function
    pub fn get_fn(&self, mut namespace: Vec<u64>, name: Box<Data>) -> &Box<Function> {
        if namespace.len() > 0 {
            let child_ns = self.child_namespaces.get(&namespace[0]).unwrap_or_else(|| error!(9901, name));
            namespace.remove(0);
            child_ns.get_fn(namespace, name)
        } else {
            self.members.get(&(name.get_data_type(), name.as_num())).unwrap_or_else(|| error!(9901, name))
        }
    }

    /// Prints all members of the namespace
    ///
    /// # Arguments
    /// * `parents` - the parent namespaces formatted as q.*
    #[cfg(debug_assertions)]
    pub fn print(&self, parents: String) {
        for child in &self.child_namespaces {
            child.1.print(format!("{}.{}", parents, num_as_q!(*child.0)));
        }
        for func in &self.members {
            println!("{}.{:?}({})", parents, (func.0).0, (func.0).1);
        }
    }
}

/// List of all possible actions to execute after a function return
pub enum FuncRet {
    /// Do nothing
    Void,
    /// Execute code from the reader
    Execute(BufReader<File>),
}

/// An executable function
pub trait Function: FuncClone {
    /// Calls the function
    ///
    /// # Arguments
    /// * `stack` - the stack to use
    /// * `heap` - the heap to use
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap);
    /// Gets the function return value
    ///
    /// # Returns
    /// The return value
    fn get_ret(&mut self) -> FuncRet {
        FuncRet::Void
    }
}

/// Wrapper for functions to make them clonable inside of boxes
pub trait FuncClone {
    /// Clones a box containing a Function
    fn clone_box(&self) -> Box<Function>;
}

impl<T> FuncClone for T where T: 'static + Function + Clone {
    fn clone_box(&self) -> Box<Function> {
        Box::new(self.clone())
    }
}

impl Clone for Box<Function> {
    fn clone(&self) -> Box<Function> {
        self.clone_box()
    }
}

/// A function containing one or more commands
#[derive(Clone)]
pub struct QRFunction {
    child_commands: Vec<Box<Command>>,
}

impl Function for QRFunction {
    fn call(&mut self, stack: &mut Stack, heap: &mut Heap) {
        for command in &self.child_commands {
            match command.execute(stack, heap) {
                Escape::None => (),
                Escape::Return => {
                    #[cfg(debug_assertions)]
                        println!("Returning from function");
                    break;
                }
                x => x.error(),
            }
        }
    }
}

impl QRFunction {
    /// Creates a new QRFunction
    ///
    /// # Arguments
    /// * `child_commands` - the commands in the function
    ///
    /// # Returns
    /// The newly created function
    pub fn new(child_commands: Vec<Box<Command>>) -> Self {
        QRFunction { child_commands }
    }
}
