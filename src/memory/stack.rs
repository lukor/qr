use memory::data::Data;
use commands::stack::PopAction;

/// A single element on a stack
pub trait StackElement {
    /// Gets the data of the element or one of the ones following it
    ///
    /// # Arguments
    /// * `index` - 0 for the data of this element, >0 for elements following it
    ///
    /// # Return
    /// A reference to the data
    fn get(&self, index: u64) -> &Box<Data>;
    /// Gets the element following this one
    ///
    /// # Returns
    /// The next element
    fn next(self: Box<Self>) -> Box<StackElement>;
    /// Takes the data from the stack element
    ///
    /// # Returns
    /// A tuple containing the data and the followup stack element
    fn take(self: Box<Self>) -> (Box<Data>, Box<StackElement>);
    /// Takes the data of some element in the stack
    ///
    /// # Arguments
    /// * `index` - 0 for the data of this element, >0 for elements following it
    ///
    /// # Returns
    /// A tuple containing the data and the followup stack element
    fn take_from(self: Box<Self>, index: u64) -> (Box<Data>, Box<StackElement>);
    /// Prints the element and all of its followup elements
    #[cfg(debug_assertions)]
    fn print(&self);
}

/// A stack element for QR
pub struct QRStackElement {
    data: Box<Data>,
    next: Box<StackElement>,
}

impl StackElement for QRStackElement {
    fn get(&self, index: u64) -> &Box<Data> {
        if index == 0 {
            &self.data
        } else {
            self.next.get(index - 1)
        }
    }

    #[cfg(debug_assertions)]
    fn print(&self) {
        print!("{} -> ", self.data);
        self.next.print();
    }
    fn next(self: Box<Self>) -> Box<StackElement> {
        self.next
    }
    fn take(self: Box<Self>) -> (Box<Data>, Box<StackElement>) {
        let slf = *self;
        let QRStackElement { data, next } = slf;
        (data, next)
    }
    fn take_from(self: Box<Self>, index: u64) -> (Box<Data>, Box<StackElement>) {
        if index == 1 {
            let (data, next) = self.take();
            let (dta, nxt) = next.take();
            (dta, Box::new(QRStackElement { data, next: nxt }))
        } else {
            let (dta, nxt) = self.take();
            let (data, next) = nxt.take_from(index - 1);
            (data, Box::new(QRStackElement { data: dta, next }))
        }
    }
}

impl QRStackElement {
    /// Creates a new stack element
    ///
    /// # Arguments
    /// * `data` - the data of the element
    /// * `next` - the followup element
    ///
    /// # Returns
    /// The newly created element
    fn new(data: Box<Data>, next: Box<StackElement>) -> Self {
        QRStackElement { data, next }
    }
}

/// A root stack element (last element in stack)
pub struct RootStackElement;

macro_rules! stack_end_read {
    () => {error!(1901)};
}

impl StackElement for RootStackElement {
    fn get(&self, _index: u64) -> &Box<Data> {
        stack_end_read!()
    }

    #[cfg(debug_assertions)]
    fn print(&self) {
        println!("<Stack end>");
    }
    fn next(self: Box<Self>) -> Box<StackElement> {
        stack_end_read!()
    }
    fn take(self: Box<Self>) -> (Box<Data>, Box<StackElement>) {
        stack_end_read!()
    }
    fn take_from(self: Box<Self>, _index: u64) -> (Box<Data>, Box<StackElement>) {
        stack_end_read!()
    }
}

/// A stack containing stack elements
pub struct Stack(Option<Box<StackElement>>);

impl Stack {
    /// Creates a new empty stack
    ///
    /// # Returns
    /// The newly created stack
    pub fn new() -> Self { Stack(Some(Box::new(RootStackElement))) }
    /// Pushes an element to the stack
    ///
    /// # Arguments
    /// * `data` - the data of the element to push
    pub fn push(&mut self, data: Box<Data>) {
        let second;
        if self.0.is_some() {
            second = self.0.take().unwrap();
        } else {
            second = Box::new(RootStackElement);
        }
        self.0 = Some(Box::new(QRStackElement::new(data, second)));
        #[cfg(debug_assertions)]
            self.print();
    }

    /// Pushes a list of elements to the stack
    ///
    /// # Arguments
    /// * `data` - the values to push
    pub fn push_multiple(&mut self, data: Vec<Box<Data>>) {
        let mut next;
        if self.0.is_some() {
            next = self.0.take().unwrap();
        } else {
            next = Box::new(RootStackElement);
        }
        for elem in data.into_iter() {
            next = Box::new(QRStackElement::new(elem, next));
        }
        self.0 = Some(next);
        #[cfg(debug_assertions)]
            self.print();
    }

    /// Pops one or more elements from the stack
    ///
    /// # Arguments
    /// * `amount` - how many elements to pop
    pub fn pop(&mut self, amount: &PopAction) {
        match amount {
            &PopAction::Many(many) => {
                for _ in 0..many {
                    self.pop_one();
                }
            }
            &PopAction::String => {
                while !self.0.as_mut().unwrap().get(0).is_null() {
                    self.pop_one();
                }
                self.pop_one();
            }
            &PopAction::Stack => {
                let amount = self.pop_one().as_num();
                for _ in 0..amount {
                    self.pop_one();
                }
            }
        }
        #[cfg(debug_assertions)]
            self.print();
    }

    /// Pops and returns a single element from the stack
    ///
    /// # Returns
    /// The data which was popped
    pub fn pop_one(&mut self) -> Box<Data> {
        if self.0.is_none() {
            stack_end_read!();
        }
        let prev_head = self.0.take().unwrap().take();
        let ret = prev_head.0;
        self.0 = Some(prev_head.1);
        ret
    }
    /// Pops a zero terminated string from the stack
    ///
    /// # Returns
    /// The string
    pub fn pop_str(&mut self) -> Vec<Box<Data>> {
        let mut ret = vec![];
        loop {
            let current = self.pop_one();
            if current.is_null() {
                return ret;
            }
            ret.push(current);
        }
    }

    /// Gets data at the given index of the stack
    ///
    /// # Arguments
    /// * `index` - the index to get data from
    ///
    /// # Returns
    /// A reference to the data
    pub fn get(&self, index: u64) -> &Box<Data> {
        if self.0.is_none() {
            stack_end_read!();
        }
        self.0.as_ref().unwrap().get(index - 1)
    }
    /// Gets a zero terminated string from the stack
    ///
    /// # Returns
    /// The string
    pub fn get_str(&self) -> Vec<&Box<Data>> {
        self.get_str_at(0)
    }
    /// Gets a zero terminated string at the given index of the stack
    ///
    /// # Arguments
    /// * `i` - the index of the string
    ///
    /// # Returns
    /// The string
    pub fn get_str_at(&self, mut i: u64) -> Vec<&Box<Data>> {
        let mut ret = vec![];
        loop {
            i += 1;
            let current = self.get(i);
            if current.is_null() {
                return ret;
            }
            ret.push(current);
        }
    }

    /// Takes the element at the given index of the stack
    ///
    /// # Arguments
    /// * `index` - the index off the element
    ///
    /// # Returns
    /// The data of the element
    pub fn take(&mut self, index: u64) -> Box<Data> {
        if self.0.is_none() {
            stack_end_read!();
        }
        let head = self.0.take().unwrap();
        let (data, stack_head) = head.take_from(index - 1);
        self.0 = Some(stack_head);
        data
    }

    /// Prints the stack
    #[cfg(debug_assertions)]
    pub fn print(&self) {
        print!("Stack: ");
        self.0.as_ref().unwrap().print();
    }
}
