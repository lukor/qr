use std::io::{BufRead, stdout, Write};
use std::error::Error;
use interpreter::Interpreter;
use std::panic;

macro_rules! prompt {
    ($print_prompt:expr) => {if $print_prompt {prompt!();}};
    () => {print!("> ");let _ = stdout().flush();};
}

/// Parses a QR program
///
/// # Arguments
/// * `reader` - the reader supplying the program
/// * `interpreter` - the interpreter to use
/// * `print_prompt` - whether to print a prompt (REPL)
pub fn parse<T: Interpreter>(reader: &mut BufRead, interpreter: &mut T, print_prompt: bool) {
    loop {
        prompt!(print_prompt);
        let mut line = String::new();
        match reader.read_line(&mut line) {
            Ok(0) => break,
            Ok(_) => (),
            Err(e) => {
                println!("Error reading input: {}", e.description());
                break;
            }
        }

        let mut current = 0u64;
        for char in line.trim().as_bytes() {
            if *char == 113u8 {
                current += 1;
            } else if *char == 32u8 {
                add_interpreter_token(interpreter, current, print_prompt);
                current = 0;
            }
        }
        add_interpreter_token(interpreter, current, print_prompt);
        if !interpreter.executed() && print_prompt {
            println!("Error: missing parameter");
            interpreter.reset();
        }
    }
}

/// Adds a token to the interpreter and catches any `panic`s
///
/// # Arguments
/// * `interpreter` - the interpreter to add the token to
/// * `token` - the token to add
/// * `recover_panic` - if panics should be fatal or not (-> interactive REPL)
fn add_interpreter_token<T: Interpreter>(interpreter: &mut T, token: u64, recover_panic: bool) {
    let result = panic::catch_unwind(panic::AssertUnwindSafe(|| match interpreter.add_token(token) {
        Some(mut reader) => parse(&mut reader, interpreter, false),
        _ => (),
    }));
    if result.is_err() {
        if !recover_panic {
            let err = result.err().unwrap();
            if let Some(msg) = err.downcast_ref::<&str>() {
                panic!("{}", msg);
            }
            panic!("{}", err.downcast::<String>().unwrap())
        } else {
            interpreter.reset();
        }
    }
}
