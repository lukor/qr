use memory::stack::Stack;
use memory::heap::Heap;
use commands::{Command, Escape};
use memory::data::Data;
use std::clone::Clone;

/// Stack command handlers
mod stack;
/// Data command handlers
mod data;
/// Primitive operation command handlers
mod ops;
/// Comparison operation command handlers
mod comp;
/// Block command handlers
mod block;
/// Cast command handlers
mod cast;

macro_rules! no_next_supported {
    () => {error!(98001);};
}

/// Wrapper for handler to make them clonable inside of boxes
pub trait HandlerClone {
    /// Clones a box containing a Handler
    fn clone_box(&self) -> Box<Handler>;
}

impl<T> HandlerClone for T where T: 'static + Handler + Clone {
    fn clone_box(&self) -> Box<Handler> {
        Box::new(self.clone())
    }
}

impl Clone for Box<Handler> {
    fn clone(&self) -> Box<Handler> {
        self.clone_box()
    }
}

/// Handler for an interpreter token
pub trait Handler: HandlerClone {
    /// Returns the next argument handler in the chain
    ///
    /// # Returns
    /// A box containing the next handler
    fn next_handler(&mut self) -> &mut Box<Handler> {
        error!(98002)
    }
    /// Returns true if there is another handler in the chain
    ///
    /// # Returns
    /// A boolean indicating whether the handler has a successor
    fn has_next_handler(&self) -> bool { false }
    /// Returns whether the handler can have another handler attached
    ///
    /// # Returns
    /// A boolean indicating if a handler can be attached to the object
    fn can_have_next_handler(&self) -> bool { false }
    /// Returns the next handler to be used in the chain
    ///
    /// # Arguments
    /// * `num` - The interpreter token to get the handler for
    ///
    /// # Returns
    /// A box containing the new handler
    fn get_next_handler(&self, _num: u64) -> Box<Handler> {
        no_next_supported!()
    }
    /// Populates the next handler in the chain
    ///
    /// # Arguments
    /// * `handler` - The handler to attach to this one
    fn populate_next_handler(&mut self, _handler: Box<Handler>) {
        no_next_supported!();
    }
    /// Returns if the handler has all information it needs
    /// so additional information can be passed on in the chain
    ///
    /// # Returns
    /// A boolean indicating if the handler has all necessary information
    fn is_complete(&self) -> bool { false }
    /// Adds an argument to the handler
    ///
    /// # Arguments
    /// * `arg` - The argument to add
    ///
    /// # Returns
    /// A boolean indicating whether the command (all handlers in the tree) is complete
    fn add_argument(&mut self, arg: u64) -> bool {
        if self.is_complete() {
            if self.can_have_next_handler() {
                if self.has_next_handler() {
                    self.next_handler().add_argument(arg)
                } else {
                    let next_handler = self.get_next_handler(arg);
                    self.populate_next_handler(next_handler);
                    self.next_handler().all_complete()
                }
            } else {
                error!(98003);
            }
        } else {
            self.add_self_argument(arg)
        }
    }
    /// Adds an argument to the handler without passing it on in the chain
    ///
    /// # Arguments
    /// * `arg` - The argument to add
    ///
    /// # Returns
    /// A boolean indicating whether the current handler is complete
    fn add_self_argument(&mut self, _arg: u64) -> bool {
        error!(98004)
    }
    /// Returns a command which can then be executed
    ///
    /// # Returns
    /// A boxed version of an executable command
    fn generate_command(self: Box<Self>) -> Box<Command> { // Cannot implement here because it's consuming -> cannot consume borrowed child handler from next_handler
        error!(98005)
    }
    /// Returns if the handler and all attached links are complete
    ///
    /// # Returns
    /// A boolean indicating whether the handler chain is complete
    fn all_complete(&mut self) -> bool {
        self.is_complete() && self.can_have_next_handler() && self.has_next_handler() && self.next_handler().all_complete()
    }
    /// Returns any data managed by the handler
    ///
    /// # Returns
    /// A vector containing boxed data
    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        error!(98006)
    }
}

/// A handler for root elements of commands
#[derive(Clone)]
pub struct RootHandler {
    next_handler: Option<Box<Handler>>,
}

impl Handler for RootHandler {
    fn next_handler(&mut self) -> &mut Box<Handler> {
        self.next_handler.as_mut().unwrap()
    }
    fn has_next_handler(&self) -> bool { self.next_handler.is_some() }
    fn can_have_next_handler(&self) -> bool { true }
    fn get_next_handler(&self, num: u64) -> Box<Handler> {
        RootHandler::get_handler_for_id(num)
    }
    fn populate_next_handler(&mut self, handler: Box<Handler>) {
        self.next_handler = Some(handler);
    }
    fn is_complete(&self) -> bool { true }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        self.next_handler.unwrap().generate_command()
    }
}

impl RootHandler {
    /// Creates a new RootHandler
    ///
    /// # Returns
    ///
    /// The newly created handler
    pub fn new() -> Self {
        RootHandler { next_handler: None }
    }

    /// Version of generate_command accepting an unboxed version of self
    ///
    /// # Returns
    /// A boxed version of an executable command
    pub fn generate_command(self) -> Box<Command> { // Since root is probably not boxed
        self.next_handler.unwrap().generate_command()
    }
    /// Generates and executes a command from the handler
    ///
    /// # Arguments
    /// * `stack` - The stack to use for executing
    /// * `heap` - The heap to use for executing
    ///
    /// # Returns
    /// An escape code
    pub fn execute(self, stack: &mut Stack, heap: &mut Heap) -> Escape { // Same here
        self.generate_command().execute(stack, heap)
    }
    /// Finds the right handler to append for an interpreter token
    ///
    /// # Arguments
    /// * `num` - The interpreter token
    ///
    /// # Returns
    /// A boxed version of a new handler
    pub fn get_handler_for_id(num: u64) -> Box<Handler> {
        match num {
            1 => Box::new(stack::StackHandler::new()),
            2 => Box::new(ops::OperationHandler::new()),
            3 => Box::new(comp::CompareHandler::new()),
            4 => Box::new(block::BlockHandler::new()),
            5 => Box::new(block::CallHandler),
            6 => error!(10122),
            7 => Box::new(cast::CastHandler::new()),
            _ => error!(10001, num_as_q!(num)),
        }
    }
}
