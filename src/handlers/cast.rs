use handlers::Handler;
use commands::Command;
use commands::cast::CastCommand;

/// Handler for casts
#[derive(Clone)]
pub struct CastHandler {
    target_type: u64,
}

impl Handler for CastHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.target_type = arg;
        true
    }
    fn is_complete(&self) -> bool {
        self.target_type > 0
    }
    fn all_complete(&mut self) -> bool {
        self.is_complete()
    }
    fn generate_command(self: Box<Self>) -> Box<Command> {
        Box::new(CastCommand::new(self.target_type))
    }
}

impl CastHandler {
    /// Creates a new CastHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        CastHandler { target_type: 0 }
    }
}
