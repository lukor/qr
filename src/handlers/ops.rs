use commands::Command;
use commands::ops::{AddCommand, SubCommand, MulCommand, DivCommand, ModCommand, AndCommand, OrCommand, XorCommand, NotCommand, PowCommand, GetDtypeCommand};
use handlers::Handler;

/// Handler for all primitive operations
#[derive(Clone)]
pub struct OperationHandler {
    operation: Operation,
}

/// All possible primitive operations
#[derive(PartialEq, Clone)]
enum Operation {
    /// Addition
    Add,
    /// Subtraction
    Subtract,
    /// Multiplication
    Multiply,
    /// Division
    Divide,
    /// Modulo operation
    Modulo,
    /// Bitwise and
    And,
    /// Bitwise or
    Or,
    /// Bitwise xor
    Xor,
    /// Bitwise not
    Not,
    /// Power
    Pow,
    /// Get datatype
    GetDtype,
    /// Operation not yet known
    Unknown,
}

impl Handler for OperationHandler {
    fn is_complete(&self) -> bool { self.operation != Operation::Unknown }
    fn all_complete(&mut self) -> bool {
        self.is_complete()
    }
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.operation = match arg {
            1 => Operation::Add,
            2 => Operation::Subtract,
            3 => Operation::Multiply,
            4 => Operation::Divide,
            5 => Operation::Modulo,
            6 => Operation::And,
            7 => Operation::Or,
            8 => Operation::Xor,
            9 => Operation::Not,
            10 => Operation::Pow,
            11 => Operation::GetDtype,
            _ => error!(10101, num_as_q!(arg)),
        };
        true
    }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        match self.operation {
            Operation::Add => Box::new(AddCommand),
            Operation::Subtract => Box::new(SubCommand),
            Operation::Multiply => Box::new(MulCommand),
            Operation::Divide => Box::new(DivCommand),
            Operation::Modulo => Box::new(ModCommand),
            Operation::And => Box::new(AndCommand),
            Operation::Or => Box::new(OrCommand),
            Operation::Xor => Box::new(XorCommand),
            Operation::Not => Box::new(NotCommand),
            Operation::Pow => Box::new(PowCommand),
            Operation::GetDtype => Box::new(GetDtypeCommand),
            _ => error!(19101),
        }
    }
}

impl OperationHandler {
    /// Creates a new OperationHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        OperationHandler { operation: Operation::Unknown }
    }
}
