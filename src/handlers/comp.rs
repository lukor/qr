use commands::Command;
use commands::comp::{NotCommand, EqCommand, NeqCommand, LtCommand, GtCommand, LteCommand, GteCommand};
use handlers::Handler;

/// Handler for comparison operations
#[derive(Clone)]
pub struct CompareHandler {
    comparison: Comparison,
}

/// All possible comparison operations
#[derive(PartialEq, Clone)]
enum Comparison {
    /// Negation
    Not,
    /// Comparison
    Equal,
    /// One element is smaller than the other
    Less,
    /// One element is larger than the other
    Greater,
    /// One element is smaller than or equal to the other
    LessEqual,
    /// One element is larger than or equal to the other
    GreaterEqual,
    /// Inversion of Equal
    NotEqual,
    /// Operation not yet known
    Unknown,
}

impl Handler for CompareHandler {
    fn is_complete(&self) -> bool { self.comparison != Comparison::Unknown }
    fn all_complete(&mut self) -> bool {
        self.is_complete()
    }
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.comparison = match arg {
            1 => Comparison::Not,
            2 => Comparison::Equal,
            3 => Comparison::Less,
            4 => Comparison::Greater,
            5 => Comparison::LessEqual,
            6 => Comparison::GreaterEqual,
            7 => Comparison::NotEqual,
            _ => error!(10111, num_as_q!(arg)),
        };
        true
    }

    fn generate_command(self: Box<Self>) -> Box<Command> {
        match self.comparison {
            Comparison::Not => Box::new(NotCommand),
            Comparison::Equal => Box::new(EqCommand),
            Comparison::NotEqual => Box::new(NeqCommand),
            Comparison::Less => Box::new(LtCommand),
            Comparison::Greater => Box::new(GtCommand),
            Comparison::LessEqual => Box::new(LteCommand),
            Comparison::GreaterEqual => Box::new(GteCommand),
            _ => error!(19101),
        }
    }
}

impl CompareHandler {
    /// Creates a new CompareHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        CompareHandler { comparison: Comparison::Unknown }
    }
}
