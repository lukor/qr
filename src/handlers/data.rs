use handlers::Handler;
use memory::data::{Data, Null, Num, Char, Namespace, Dtype};

/// Offset for the ascii value of lower case characters
const LOWER_OFFSET: u8 = 96;
/// Offset for the ascii value of upper case characters
const UPPER_OFFSET: u8 = 64;
/// Special characters
const SPECIAL_CHARS: &str = "\n !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
/// Offset for the ascii value of numbers
const NUM_OFFSET: u8 = 47;

/// Handler for any kind of data
#[derive(Clone)]
pub struct DataHandler {
    pub data: Option<Box<Null>>,
    next_handler: Option<Box<Handler>>,
}

impl Handler for DataHandler {
    fn next_handler(&mut self) -> &mut Box<Handler> {
        self.next_handler.as_mut().unwrap()
    }
    fn has_next_handler(&self) -> bool { self.next_handler.is_some() }
    fn can_have_next_handler(&self) -> bool { true }
    fn is_complete(&self) -> bool { self.has_next_handler() }

    fn add_self_argument(&mut self, arg: u64) -> bool {
        if self.data.is_some() {
            self.data.as_mut().unwrap().add_data(arg);
        } else {
            if arg == 1 {
                self.data = Some(Box::new(Null));
            } else {
                self.next_handler = Some(Self::get_data_handler(arg));
            }
        }
        self.all_complete()
    }
    fn all_complete(&mut self) -> bool {
        self.data.is_some() && self.data.as_ref().unwrap().is_complete() || self.next_handler.is_some() && self.next_handler.as_mut().unwrap().all_complete() || false
    }
    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        if self.data.is_some() {
            vec![self.data.unwrap()]
        } else {
            self.next_handler.unwrap().get_data()
        }
    }
}

impl DataHandler {
    /// Creates a new DataHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        DataHandler { data: None, next_handler: None }
    }

    /// Finds the handler for the correct data type
    ///
    /// # Arguments
    /// * `num` - The interpreter token to find the data type for
    ///
    /// # Returns
    /// A boxed version of a handler for the given data type
    pub fn get_data_handler(num: u64) -> Box<Handler> {
        match num {
            2 => Box::new(NumHandler::new()),
            3 => Box::new(CharHandler::new()),
            4 => Box::new(ArrHandler::new()),
            5 => Box::new(StringHandler::new()),
            6 => Box::new(NamespaceHandler::new()),
            7 => Box::new(DatatypeHandler::new()),
            _ => error!(10201, num_as_q!(num)),
        }
    }
}

/// A handler for any number
#[derive(Clone)]
struct NumHandler {
    data: Num,
    is_neg: Option<bool>,
}

impl Handler for NumHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        if self.is_neg.is_none() {
            self.is_neg = Some(match arg {
                1 => false,
                2 => true,
                _ => error!(10211),
            });
        } else {
            self.data.add_data_neg(if self.is_neg.unwrap() { -(arg as i64) } else { (arg-1) as i64 });
        }
        self.all_complete()
    }
    fn add_argument(&mut self, arg: u64) -> bool {
        self.add_self_argument(arg)
    }
    fn all_complete(&mut self) -> bool {
        self.data.is_complete()
    }
    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        vec![Box::new(self.data)]
    }
    fn is_complete(&self) -> bool {
        self.is_neg.is_some()
    }
}

impl NumHandler {
    /// Creates a new NumHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        NumHandler { data: Num::new(), is_neg: None }
    }
}

/// Types of characters
#[derive(PartialEq, Clone)]
enum CharClass {
    /// Class not yet known
    Unknown,
    /// Lowercase characters
    Lower,
    /// Uppercase characters
    Upper,
    /// Special characters
    Special,
    /// Numbers
    Number,
    /// Any character specified by ascii code
    Ascii,
}

/// Handler for all characters
#[derive(Clone)]
struct CharHandler {
    data: Char,
    class: CharClass,
}

impl Handler for CharHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        if self.class == CharClass::Unknown {
            self.class = match arg {
                1 => CharClass::Lower,
                2 => CharClass::Upper,
                3 => CharClass::Special,
                4 => CharClass::Number,
                5 => CharClass::Ascii,
                _ => error!(10221),
            };
        } else {
            self.data.add_data_char(CharHandler::get_char(arg, &self.class));
        }
        self.all_complete()
    }
    fn add_argument(&mut self, arg: u64) -> bool {
        self.add_self_argument(arg)
    }
    fn all_complete(&mut self) -> bool {
        self.data.is_complete()
    }
    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        vec![Box::new(self.data)]
    }
    fn is_complete(&self) -> bool {
        self.class != CharClass::Unknown
    }
}

impl CharHandler {
    /// Creates a new CharHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        CharHandler { data: Char::new(), class: CharClass::Unknown }
    }

    /// Finds the correct character for the given parameters
    ///
    /// # Arguments
    /// * `num` - The character number
    /// * `class` - The character class
    ///
    /// # Returns
    /// The character
    fn get_char(num: u64, class: &CharClass) -> char {
        let n = num as u8;
        match class {
            &CharClass::Lower => (n + LOWER_OFFSET) as char,
            &CharClass::Upper => (n + UPPER_OFFSET) as char,
            &CharClass::Special => SPECIAL_CHARS.chars().nth((n - 1u8) as usize).unwrap(),
            &CharClass::Number => (n + NUM_OFFSET) as char,
            &CharClass::Ascii => n as char,
            _ => error!(19221),
        }
    }
}

/// Handler for arrays
#[derive(Clone)]
struct ArrHandler {
    dtype: Option<Box<Handler>>,
    len: u64,
    data: Vec<Box<Handler>>,
}

impl Handler for ArrHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        enum Action {
            CreateHandler,
            FillHandler,
            FillArray,
            None,
        }
        let mut _action = Action::None;
        match &self.dtype {
            &Some(ref dt) => {
                if !dt.is_complete() {
                    _action = Action::FillHandler;
                } else if self.len == 0 {
                    self.len = arg;
                    return false;
                } else {
                    _action = Action::FillArray;
                }
            }
            _ => {
                _action = Action::CreateHandler;
            }
        }
        match _action {
            Action::CreateHandler => {
                self.dtype = Some(DataHandler::get_data_handler(arg));
                if self.dtype.as_ref().unwrap().is_complete() {
                    self.data.push(self.dtype.clone().unwrap());
                }
            }
            Action::FillHandler => {
                self.dtype.as_mut().unwrap().add_self_argument(arg);
                if self.dtype.as_ref().unwrap().is_complete() {
                    self.data.push(self.dtype.clone().unwrap());
                }
            }
            Action::FillArray => {
                let last_ind = self.data.len() - 1;
                self.data[last_ind].add_self_argument(arg);
                if self.data[last_ind].all_complete() {
                    if self.data.len() == self.len as usize {
                        return true;
                    }
                    self.data.push(self.dtype.clone().unwrap());
                }
            }
            _ => (),
        }
        false
    }

    fn all_complete(&mut self) -> bool {
        self.len > 0 && self.data.len() == self.len as usize && self.data[self.len as usize - 1].all_complete()
    }

    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        self.data.into_iter().flat_map(|d| d.get_data()).rev().collect::<Vec<_>>()
    }
}

impl ArrHandler {
    /// Creates a new ArrHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        ArrHandler { dtype: None, len: 0, data: vec![] }
    }
}

/// Handler for strings
#[derive(Clone)]
struct StringHandler {
    data: Vec<Box<Handler>>,
    finished: bool,
}

impl Handler for StringHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        if self.data.len() == 0 {
            if arg != 6 {
                self.add_char();
            } else {
                self.finished = true;
                return true;
            }
        }
        let mut last_ind = self.data.len() - 1;
        if self.data[last_ind].all_complete() {
            if arg == 6 {
                self.finished = true;
                return true;
            }
            self.add_char();
            last_ind += 1;
        }
        self.data[last_ind].add_argument(arg);
        false
    }

    fn all_complete(&mut self) -> bool {
        self.finished
    }

    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        let null: Vec<Box<Data>> = vec![Box::new(Null)];
        self.data.into_iter().flat_map(|d| d.get_data()).chain(null.into_iter()).rev().collect::<Vec<_>>()
    }
}

impl StringHandler {
    /// Creates a new StringHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        StringHandler { data: vec![], finished: false }
    }

    /// Adds an empty character to the string
    fn add_char(&mut self) {
        self.data.push(Box::new(CharHandler::new()));
    }
}

/// Handler for namespaces
#[derive(Clone)]
struct NamespaceHandler {
    data: Namespace,
}

impl Handler for NamespaceHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.data.add_data(arg)
    }
    fn add_argument(&mut self, arg: u64) -> bool {
        self.add_self_argument(arg)
    }
    fn all_complete(&mut self) -> bool {
        self.data.is_complete()
    }
    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        vec![Box::new(self.data)]
    }
    fn is_complete(&self) -> bool {
        true
    }
}

impl NamespaceHandler {
    /// Creates a new NamespaceHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        NamespaceHandler { data: Namespace::new() }
    }
}

/// Handler for the `DataType` data type
#[derive(Clone)]
struct DatatypeHandler {
    data: Dtype,
}

impl Handler for DatatypeHandler {
    fn add_self_argument(&mut self, arg: u64) -> bool {
        self.data.add_data(arg)
    }
    fn add_argument(&mut self, arg: u64) -> bool {
        self.add_self_argument(arg)
    }
    fn all_complete(&mut self) -> bool {
        self.data.is_complete()
    }
    fn get_data(self: Box<Self>) -> Vec<Box<Data>> {
        vec![Box::new(self.data)]
    }
    fn is_complete(&self) -> bool {
        true
    }
}

impl DatatypeHandler {
    /// Creates a new DatatypeHandler
    ///
    /// # Returns
    /// The newly created handler
    pub fn new() -> Self {
        DatatypeHandler { data: Dtype::new() }
    }
}
