macro_rules! as_error {
    ($err_code:expr, $message:expr) => {{
        use memory::data::Error;
        Box::new(Error::from($err_code, $message))
    }};
}

macro_rules! error {
    (1001, $param:expr) => {fatal_error!(1001, format!("Invalid get index: {}", $param))};
    (1002, $param:expr) => {fatal_error!(1002, format!("Invalid move index: {}", $param))};
    (1012, $param:expr) => {fatal_error!(1012, format!("Invalid parameter for pop: {}", $param))};
    (1101, $param:expr) => {fatal_error!(1101, format!("Invalid cast target '{}'", $param))};
    (1901) => {fatal_error!(1901, "Tried to read past stack end")};

    (2001) => {fatal_error!(2001, "Cannot perform maths on Error")};
    (2002, $first:expr, $second:expr) => {incompatible_math!(2002, "multiply", $first, $second)};
    (2003, $first:expr, $second:expr) => {incompatible_math!(2003, "divide", $first, $second)};
    (2004, $first:expr, $second:expr) => {incompatible_math!(2004, "and", $first, $second)};
    (2004) => {invalid_decimal_operation!(2004, "and")};
    (2005, $first:expr, $second:expr) => {incompatible_math!(2005, "or", $first, $second)};
    (2005) => {invalid_decimal_operation!(2005, "or")};
    (2006, $first:expr, $second:expr) => {incompatible_math!(2006, "xor", $first, $second)};
    (2006) => {invalid_decimal_operation!(2006, "xor")};
    (2007, $val:expr) => {incompatible_math!(2007, "negate", $val)};
    (2007) => {invalid_decimal_operation!(2007, "negate")};
    (2008, $val:expr) => {fatal_error!(2008, format!("Can only negate bytes, '{}' is outside of byte range 0..255", $val))};
    (2009) => {fatal_error!(2009, "Please use the pow library function (q.q.a) to calculate powers of decimal numbers")};
    (2010, $val:expr) => {invalid_cast!(2010, $val, "number")};
    (2011, $val:expr) => {invalid_cast!(2011, $val, "character")};
    (2012, $val:expr) => {fatal_error!(2012, format!("{:?} is not a namespace", $val))};
    (2013, $val:expr) => {fatal_error!(2013, format!("{} is outside of byte range (0-255), cannot be converted to char", $val))};
    (2014, $val:expr) => {fatal_error!(2014, format!("{:?} does not have a string associated with it", $val))};
    (2021) => {fatal_error!(2021, "NULL does not have data associated with it")};
    (2022) => {fatal_error!(2022, "Cannot compare NULL")};
    (2023) => {fatal_error!(2023, "Cannot negate NULL")};
    (2024) => {fatal_error!(2024, "Cannot add data to error")};
    (2031, $to:expr) => {invalid_compare!(2031, "number", $to)};
    (2032, $to:expr) => {invalid_compare!(2032, "character", $to)};
    (2033, $to:expr) => {invalid_compare!(2033, "boolean", $to)};
    (2034, $to:expr) => {invalid_compare!(2034, "namespace", $to)};
    (2035, $to:expr) => {invalid_compare!(2035, "float", $to)};
    (2036, $to:expr) => {invalid_compare!(2036, "error", $to)};

    (3101, $fid:expr) => {fatal_error!(3101, format!("File with descriptor {} does not exist", $fid))};
    (3102, $fid:expr) => {as_error!(3102, format!("File with descriptor {} does not exist and could not be closed", $fid))};
    (3111, $fname:expr, $err:expr) => {as_error!(3111, format!("Could not open {}: {}", $fname, $err))};
    (3112, $param:expr) => {fatal_error!(3112, format!("Invalid file open mode: {}", $param))};
    (3112, $err:expr) => {as_error!(3112, format!("Could not read file: {}", $err))};
    (3113, $fid:expr, $err:expr) => {as_error!(3113, format!("Could not write to file {}: {}", $fid, $err))};
    (3199, $param:expr) => {fatal_error!(3199, format!("Could not open file: {}", $param))};
    (3201, $port:expr) => {fatal_error!(3201, format!("Udp socket at port {} does not exist", $port))};
    (3202, $port:expr) => {as_error!(3202, format!("Udp socket at port {} does not exist and could not be closed", $port))};
    (3211, $err:expr) => {as_error!(3211, format!("Could not open socket: {}", $err))};
    (3212, $addr:expr, $port:expr, $err:expr) => {as_error!(3212, format!("Could not send to {}:{}: {}", $addr, $port, $err))};
    (3213, $err:expr) => {as_error!(3213, format!("No data received: {}", $err))};
    (3301, $port:expr) => {fatal_error!(3301, format!("Tcp listener at port {} does not exist", $port))};
    (3302, $id:expr) => {fatal_error!(3302, format!("Tcp stream with id {} does not exist", $id))};
    (3303, $id:expr) => {as_error!(3303, format!("Tcp stream with id {} does not exist and could not be closed", $id))};
    (3311, $addr:expr, $port:expr, $err:expr) => {as_error!(3311, format!("Could not connect to {}:{}: {}", $addr, $port, $err))};
    (3312, $port:expr, $err:expr) => {as_error!(3312, format!("Could not listen on port {}: {}", $port, $err))};
    (3313, $err:expr) => {as_error!(3313, format!("Could not send data to tcp stream: {}", $err))};
    (3314, $err:expr) => {as_error!(3314, format!("Could not receive data: {}", $err))};
    (3315, $port:expr, $err:expr) => {as_error!(3315, format!("Could not get connection on port {}: {}", $port, $err))};
    (3401, $err:expr) => {as_error!(3401, format!("Could not read from stdin: {}", $err))};

    (9001) => {fatal_error!(9001, "Cannot write to system namespace ('q.*')")};
    (9011, $param:expr) => {fatal_error!(9011, format!("{:?} can not be used as a function name", $param))};
    (9021, $param:expr) => {fatal_error!(9021, format!("Unknown block control code '{}'", $param))};
    (9022, $param:expr) => {fatal_error!(9022, format!("Illegal block type '{}'", $param))};
    (9101) => {fatal_error!(9101, "Tried to continue out of root")};
    (9102) => {fatal_error!(9102, "Tried to break out of root")};
    (9103) => {fatal_error!(9103, "Tried to return outside of a function")};
    (9901, $param:expr) => {fatal_error!(9901, format!("Function {} does not exist in the given namespace", $param))};

    (10001, $param:expr) => {fatal_error!(10001, format!("Unknown command: '{}'", $param))};
    (10101, $param:expr) => {fatal_error!(10101, format!("Unknown operation: '{}'", $param))};
    (10111, $param:expr) => {fatal_error!(10111, format!("Unknown comparison operation: '{}'", $param))};
    (10121, $param:expr) => {fatal_error!(10121, format!("Unknown stack operation: '{}'", $param))};
    (10122) => {fatal_error!(10122, format!("Cannot use block commands outside of blocks"))};
    (10201, $param:expr) => {fatal_error!(10201, format!("Unknown data type {}", $param))};
    (10211) => {fatal_error!(10211, "Invalid argument for number")};
    (10221) => {fatal_error!(10221, "Invalid argument for char")};

    (19101) => {interpreter_error!(19101, "No operation specified")};
    (19221) => {interpreter_error!(19221, "No character class specified")};

    (90001) => {interpreter_error!(90001, "Did not terminate after fatal error")};

    (91011) => {interpreter_error!(91011, "Invalid pop action")};
    (91012) => {interpreter_error!(91012, "Tried to add argument to pop without specifying many")};

    (92001) => {interpreter_error!(92001, "Unknown operation error code")};

    (98001) => {interpreter_error!(98001, "This handler does not support automatically attaching another handler")};
    (98002) => {interpreter_error!(98002, "No next handler present")};
    (98003) => {interpreter_error!(98003, "Cannot automatically progress in chain")};
    (98004) => {interpreter_error!(98004, "Handler does not take arguments")};
    (98005) => {interpreter_error!(98005, "Handler does not support generating a command")};
    (98006) => {interpreter_error!(98006, "Handler does not have any data")};

    (99001) => {interpreter_error!(99001, "Tried to write to empty namespace")};
    (99101) => {interpreter_error!(99101, "Unknown block return")};
}

macro_rules! incompatible_math {
    ($err_code:expr, $operation:expr, $first:expr, $second:expr) => {
        fatal_error!($err_code, format!("Cannot {} {:?} and {:?}", $operation, $first, $second))
    };
    ($err_code:expr, $operation:expr, $val:expr) => {
        fatal_error!($err_code, format!("Cannot {} {:?}", $operation, $val))
    };
}

macro_rules! invalid_cast {
    ($err_code:expr, $val:expr, $target:expr) => {
        fatal_error!($err_code, format!("{:?} cannot be represented as a {}", $val, $target))
    };
}

macro_rules! invalid_decimal_operation {
    ($err_code:expr, $operation:expr) => {
        fatal_error!($err_code, format!("Cannot {} decimal number", $operation))
    };
}

macro_rules! invalid_compare {
    ($err_code:expr, $val:expr, $to:expr) => {
        fatal_error!($err_code, format!("Cannot compare {} to {:?}", $val, $to))
    };
}

macro_rules! interpreter_error {
    ($err_code:expr, $message:expr) => {fatal_error!($err_code, format!("Interpreter error: {}", $message))};
}

macro_rules! fatal_error {
    ($err_code:expr, $message:expr) => {panic!("Error {}: {}", $err_code, $message)};
}
