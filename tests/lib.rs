extern crate qr;

#[macro_use]
#[cfg(test)]
mod helper;

#[cfg(test)]
mod integration;
