use qr::memory::data::DataType;

#[test]
fn simple() {
    let command = "q q qqqqq q q q q qqqqqq - push error message\
                        q q qq q qqqqqqq         - push error code 6\
                        q q qqq q qqqq           - push name d\
                        q q qqqqqq q q           - push ns 1\
                        qqqqq                    - call";
    let expected = [6];
    assert_stack!(command, expected, DataType::Error, as_num, true);
}

#[test]
#[should_panic(expected="Error 6: aa")]
fn fatal() {
    let command = "q q qqqqq q q q q qqqqqq - push error message\
                        q q qq q qqqqqqq         - push error code 6\
                        q q qqq q qqqqq          - push name e\
                        q q qqqqqq q q           - push ns 1\
                        qqqqq                    - call";
    execute_command!(command);
}
