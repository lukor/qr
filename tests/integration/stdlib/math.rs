use qr::memory::data::DataType;
use std::f64::consts::{SQRT_2, LN_2};

#[test]
fn pow() {
    let command = "q q qq q qqq      - push 2\
                        q q qq q qqqq     - push 3\
                        q q qqq q q       - push name a\
                        q q qqqqqq qq q q - push ns 1.1\
                        qqqqq             - call";
    let expected = [8.0];
    assert_stack!(command, expected, DataType::Float, as_float, true);
}

#[test]
fn pow_neg() {
    let command = "q q qq q qqq      - push 2\
                        q q qq qq qqq     - push -3\
                        q q qqq q q       - push name a\
                        q q qqqqqq qq q q - push ns 1.1\
                        qqqqq             - call";
    let expected = [0.125];
    assert_stack!(command, expected, DataType::Float, as_float, true);
}

#[test]
fn pow_decimal() {
    let command = "q q qq q qqq      - push 2\
                        q q qq q qqqq     - push 3\
                        q q qq q qqq      - push 2\
                        qqqqqqq qqqqq     - cast to float\
                        qq qqqq           - div\
                        q q qqq q q       - push name a\
                        q q qqqqqq qq q q - push ns 1.1\
                        qqqqq             - call";
    let expected = [SQRT_2 * 2.0];
    assert_stack!(command, expected, DataType::Float, as_float, true);
}

#[test]
fn mod_pow() {
    let command = "q q qq q qqq      - push 2\
                        q q qq q qqqq     - push 3\
                        q q qq q qqqqqq   - push 5\
                        q q qqq q qq      - push name b\
                        q q qqqqqq qq q q - push ns 1.1\
                        qqqqq             - call";
    let expected = [3];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn sqrt() {
    let command = "q q qq q qqq      - push 2\
                        q q qqq q qqq     - push name c\
                        q q qqqqqq qq q q - push ns 1.1\
                        qqqqq             - call";
    let expected = [SQRT_2];
    assert_stack!(command, expected, DataType::Float, as_float, true);
}

#[test]
fn round_down() {
    let command = "q q qq q qq         - push 1\
                        q q qq q qqqq       - push 3\
                        qqqqqqq qqqqq       - cast to float\
                        qq qqqq             - div\
                        q q qqq q qqqq      - push name d\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn round_up() {
    let command = "q q qq q qqq        - push 2\
                        q q qq q qqqq       - push 3\
                        qqqqqqq qqqqq       - cast to float\
                        qq qqqq             - div\
                        q q qqq q qqqq      - push name d\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn ceil_down() {
    let command = "q q qq q qq         - push 1\
                        q q qq q qqqq       - push 3\
                        qqqqqqq qqqqq       - cast to float\
                        qq qqqq             - div\
                        q q qqq q qqqqq     - push name e\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn ceil_up() {
    let command = "q q qq q qqq        - push 2\
                        q q qq q qqqq       - push 3\
                        qqqqqqq qqqqq       - cast to float\
                        qq qqqq             - div\
                        q q qqq q qqqqq     - push name e\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn floor_down() {
    let command = "q q qq q qq         - push 1\
                        q q qq q qqqq       - push 3\
                        qqqqqqq qqqqq       - cast to float\
                        qq qqqq             - div\
                        q q qqq q qqqqqq    - push name f\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn floor_up() {
    let command = "q q qq q qqq        - push 2\
                        q q qq q qqqq       - push 3\
                        qqqqqqq qqqqq       - cast to float\
                        qq qqqq             - div\
                        q q qqq q qqqqqq    - push name f\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn ln() {
    let command = "q q qq q qqq        - push 2\
                        q q qqq q qqqqqqq   - push name g\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [LN_2];
    assert_stack!(command, expected, DataType::Float, as_float, true);
}

#[test]
fn log10() {
    let command = "q q qq q qqq        - push 2\
                        q q qqq q qqqqqqqq  - push name h\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [0.3010299956639812];
    assert_stack!(command, expected, DataType::Float, as_float, true);
}

#[test]
fn abs_pos() {
    let command = "q q qq q qqq        - push 2\
                        q q qqq q qqqqqqqqq - push name i\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [2];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn abs_neg() {
    let command = "q q qq qq qq        - push 2\
                        q q qqq q qqqqqqqqq - push name i\
                        q q qqqqqq qq q q   - push ns 1.1\
                        qqqqq               - call";
    let expected = [2];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

mod trig {
    use qr::memory::data::DataType;

    #[test]
    fn sin() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q q             - push name a\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [0.9092974268256817];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn cos() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qq            - push name b\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [-0.4161468365471424];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn tan() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqq           - push name c\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [-2.185039863261519];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn asin() {
        let command = "q q qq q qqq            - push 2\
                            q q qq q qqqq           - push 3\
                            qqqqqqq qqqqq           - cast to float\
                            qq qqqq                 - div\
                            q q qqq q qqqq          - push name d\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [0.7297276562269663];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn acos() {
        let command = "q q qq q qqq            - push 2\
                            q q qq q qqqq           - push 3\
                            qqqqqqq qqqqq           - cast to float\
                            qq qqqq                 - div\
                            q q qqq q qqqqq         - push name e\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [0.8410686705679303];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn asin_invalid() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqq          - push name d\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [];
        let mut interpreter = assert_stack!(command, expected, DataType::Float, as_float, false);
        assert!(interpreter.stack.pop_one().as_float().is_nan());
    }

    #[test]
    fn acos_invalid() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqq         - push name e\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [];
        let mut interpreter = assert_stack!(command, expected, DataType::Float, as_float, false);
        assert!(interpreter.stack.pop_one().as_float().is_nan());
    }

    #[test]
    fn atan() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqq        - push name f\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [1.1071487177940904];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn sinh() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqqq       - push name g\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [3.626860407847019];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn cosh() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqqqq      - push name h\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [3.7621956910836314];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn tanh() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqqqqq     - push name i\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [0.9640275800758169];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn asinh() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqqqqqq    - push name j\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [1.4436354751788103];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn acosh() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqqqqqqq   - push name k\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [1.3169578969248166];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn atanh() {
        let command = "q q qq q qqq            - push 2\
                            q q qq q qqqq           - push 3\
                            qqqqqqq qqqqq           - cast to float\
                            qq qqqq                 - div\
                            q q qqq q qqqqqqqqqqqq  - push name l\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [0.8047189562170501];
        assert_stack!(command, expected, DataType::Float, as_float, true);
    }

    #[test]
    fn atanh_invalid() {
        let command = "q q qq q qqq            - push 2\
                            q q qqq q qqqqqqqqqqqq  - push name l\
                            q q qqqqqq qqq q q q    - push ns 1.1\
                            qqqqq                   - call";
        let expected = [];
        let mut interpreter = assert_stack!(command, expected, DataType::Float, as_float, false);
        assert!(interpreter.stack.pop_one().as_float().is_nan());
    }
}
