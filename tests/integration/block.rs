use qr::memory::data::DataType;

#[test]
#[should_panic(expected="Error 1901")]
fn if_true() {
    let command = "q q qq q qq - push 1\
                        qqqq q      - if\
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q    - fi";
    execute_command!(command);
}

#[test]
fn if_false() {
    let command = "q q qq q q  - push 0\
                        qqqq q      - if\
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q    - fi";
    execute_command!(command);
}

#[test]
fn while_continuous() {
    let command = "q q qq q qqq - push 2\
                        qqqq qq      - while\
                            q q qq q qq - push 1\
                            qq qq       - sub\
                        qqqqqq q     - end";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn while_immediate_false() {
    let command = "q q qq q q - push 0\
                        qqqq qq    - while\
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q   - end";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn while_continue() {
    let command = "q q qq q qqq - push 2\
                        qqqq qq      - while\
                            q q qq q qq   - push 1\
                            qq qq         - sub\
                            qqqqqq qq q   - continue\
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q     - end";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn while_break() {
    let command = "q q qq q qqq - push 2\
                        qqqq qq      - while\
                            q q qq q qq   - push 1\
                            qq qq         - sub\
                            qqqqqq qqq q  - break\
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q     - end";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn continue_multiple() {
    let command = "q q qq q qqq - push 2\
                        qqqq qq      - while\
                            q q qq q qq   - push 1\
                            qqqq qq       - while\
                                qq qq         - sub\
                                qqqqqq qq qq  - continue 2\
                                q qq qq qqqqq - pop 5 - throws error\
                            qqqqqq q      - end
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q     - end";
    let expected = [0];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
fn break_multiple() {
    let command = "q q qq q qqq - push 2\
                        qqqq qq      - while\
                            q q qq q qq   - push 1\
                            qq qq         - sub\
                            qqqq qq       - while\
                                qqqqqq qqq qq - break 2\
                                q qq qq qqqqq - pop 5 - throws error\
                            qqqqqq q      - end
                            q qq qq qqqqq - pop 5 - throws error\
                        qqqqqq q     - end";
    let expected = [1];
    assert_stack!(command, expected, DataType::Num, as_num, true);
}

#[test]
#[should_panic(expected="9101")]
fn continue_root() {
    let command = "q q qq q qq - push 1\
                        qqqq qq     - while\
                            qqqqqq qq qq - continue 2\
                        qqqqqq q    - end";
    execute_command!(command);
}

#[test]
#[should_panic(expected="9102")]
fn break_root() {
    let command = "q q qq q qq - push 1\
                        qqqq qq     - while\
                            qqqqqq qqq qq - continue 2\
                        qqqqqq q    - end";
    execute_command!(command);
}

#[test]
#[should_panic(expected="10122")]
fn block_command_in_root() {
    let command = "qqqqqq - block command";
    execute_command!(command);
}

mod function {
    use qr::memory::data::{DataType, Num};
    use qr::memory::heap::{Heap, Function};
    use qr::memory::stack::Stack;

    #[derive(Clone)]
    struct TestFunction;

    impl Function for TestFunction {
        fn call(&mut self, _stack: &mut Stack, _heap: &mut Heap) {
            panic!("Function executed");
        }
    }

    #[test]
    fn create_empty_named() {
        let command = "q q qq q qq         - push fn name 1\
                            q q qqqqqq qq qq q  - push ns 2.1\
                            qqqq qqq            - function start\
                            qqqqqq q            - function end";

        let ns = vec![2u64, 1u64];
        let mut interpreter;

        {
            let expected_ns = [&ns];
            let expected = [1];

            interpreter = assert_stack!(command, expected_ns, DataType::Namespace, as_ns, false);
            assert_stack_values!(interpreter, expected, DataType::Num, as_num);
            assert_stack_end!(interpreter);
        }

        let mut function = interpreter.heap.get_fn(ns, Box::new(Num::from(1))).clone();
        function.call(&mut interpreter.stack, &mut interpreter.heap);
        assert_stack_end!(interpreter);
    }

    #[test]
    fn create_named() {
        let command = "q q qq q qq         - push fn name 1\
                            q q qqqqqq qq qq q  - push ns 2.1\
                            qqqq qqq            - function start\
                                q q qq q qqq        - push 2\
                            qqqqqq q            - function end";

        let ns = vec![2u64, 1u64];
        let mut interpreter;

        {
            let expected_ns = [&ns];
            let expected = [1];

            interpreter = assert_stack!(command, expected_ns, DataType::Namespace, as_ns, false);
            assert_stack_values!(interpreter, expected, DataType::Num, as_num);
            assert_stack_end!(interpreter);
        }

        let mut function = interpreter.heap.get_fn(ns, Box::new(Num::from(1))).clone();
        function.call(&mut interpreter.stack, &mut interpreter.heap);
        let expected_in_fn = [2];
        assert_stack_values!(interpreter, expected_in_fn, DataType::Num, as_num);
        assert_stack_end!(interpreter);
    }

    #[test]
    #[should_panic(expected = "Error 9001")]
    fn create_in_system_ns() {
        let command = "q q qq q qq         - push fn name 1\
                            q q qqqqqq q q      - push ns 1\
                            qqqq qqq            - function start\
                            qqqqqq q            - function end";
        execute_command!(command);
    }

    #[test]
    fn create_empty_anonymous() {
        let command = "qqqq qqqq           - anon function start\
                            qqqqqq q            - function end";

        let ns = vec![1u64];
        let mut interpreter;
        let fn_name;

        {
            let expected_ns = [&ns];

            interpreter = assert_stack!(command, expected_ns, DataType::Namespace, as_ns, false);
            fn_name = interpreter.stack.pop_one().as_num();
            assert_stack_end!(interpreter);
        }

        let mut function = interpreter.heap.get_fn(ns, Box::new(Num::from(fn_name))).clone();
        function.call(&mut interpreter.stack, &mut interpreter.heap);
        assert_stack_end!(interpreter);
    }

    #[test]
    fn create_anonymous() {
        let command = "qqqq qqqq           - anon function start\
                                q q qq q qqq        - push 2\
                            qqqqqq q            - function end";

        let ns = vec![1u64];
        let mut interpreter;
        let fn_name;

        {
            let expected_ns = [&ns];

            interpreter = assert_stack!(command, expected_ns, DataType::Namespace, as_ns, false);
            fn_name = interpreter.stack.pop_one().as_num();
            assert_stack_end!(interpreter);
        }

        let mut function = interpreter.heap.get_fn(ns, Box::new(Num::from(fn_name))).clone();
        function.call(&mut interpreter.stack, &mut interpreter.heap);
        let expected_in_fn = [2];
        assert_stack_values!(interpreter, expected_in_fn, DataType::Num, as_num);
        assert_stack_end!(interpreter);
    }

    #[test]
    #[should_panic(expected="Function executed")]
    fn call() {
        use qr::interpreter::QRInterpreter;

        let mut interpreter = QRInterpreter::new();
        let command = "q q qq q qq     - push 1\
                            q q qqqqqq q qq - push ns 2\
                            qqqqq           - call";

        let function = TestFunction;
        interpreter.heap.add_fn(vec![2], Box::new(Num::from(1)), Box::new(function));

        execute!(command, &mut interpreter);
    }

    #[test]
    #[should_panic(expected="Error 9901")]
    fn call_nonexistent() {
        let command = "q q qq q qq     - push 1\
                            q q qqqqqq q qq - push ns 2\
                            qqqqq           - call";
        execute_command!(command);
    }
}
