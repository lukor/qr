use qr::memory::data::DataType;

#[test]
fn not_true() {
    let command = "q q qq q qq - push 1\
                        qqq q       - not";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn not_false() {
    let command = "q q qq q q - push 0\
                        qqq q      - not";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn equals_for_true() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqqq - push 3\
                        qqq qq        - e\\";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn equals_for_false() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qqq qq        - e\\";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn less_for_greater() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qqq qqq       - lt";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn less_for_eq() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqqq - push 3\
                        qqq qqq       - lt";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn less_for_less() {
    let command = "q q qq q qqq  - push 2\
                        q q qq q qqqq - push 3\
                        qqq qqq       - lt";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn greater_for_greater() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qqq qqqq      - gt";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn greater_for_eq() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqqq - push 3\
                        qqq qqqq      - gt";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn greater_for_less() {
    let command = "q q qq q qqq  - push 2\
                        q q qq q qqqq - push 3\
                        qqq qqqq      - gt";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn less_or_equals_for_greater() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qqq qqqqq     - lte";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn less_or_equals_for_eq() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqqq - push 3\
                        qqq qqqqq     - lte";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn less_or_equals_for_less() {
    let command = "q q qq q qqq  - push 2\
                        q q qq q qqqq - push 3\
                        qqq qqqqq     - lte";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn greater_or_equals_for_greater() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqq  - push 2\
                        qqq qqqqqq    - gte";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn greater_or_equals_for_eq() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqqq - push 3\
                        qqq qqqqqq    - gte";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn greater_or_equals_for_less() {
    let command = "q q qq q qqq  - push 2\
                        q q qq q qqqq - push 3\
                        qqq qqqqqq    - gte";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn not_equals_for_true() {
    let command = "q q qq q qqq  - push 2\
                        q q qq q qqqq - push 3\
                        qqq qqqqqqq   - ne\\";
    let expected = [true];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}

#[test]
fn not_equals_for_false() {
    let command = "q q qq q qqqq - push 3\
                        q q qq q qqqq - push 3\
                        qqq qqqqqqq   - ne\\";
    let expected = [false];
    assert_stack!(command, expected, DataType::Bool, as_bool, true);
}
